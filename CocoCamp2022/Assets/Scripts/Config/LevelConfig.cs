using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Config
{
    [CreateAssetMenu(fileName = "LevelConfig", menuName = "CreateConfig/LevelConfig")]
    [Serializable]
    public class LevelConfig : ScriptableObject
    {
        [Header("所有关卡")]
        public LevelConfigItem[] levelList;
    }

    [Serializable]
    public class LevelConfigItem
    {
        [Header("场景名字")]
        public string sceneName;
    }
}

