using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Logic;
using System.Linq;

namespace Config
{
    [CreateAssetMenu(fileName = "CardConfig", menuName = "CreateConfig/CardConfig")]
    [Serializable]
    public class CardConfig : ScriptableObject
    {
        [Header("卡牌数量")]
        public int cardCount = 3;
        [Header("所有卡牌")]
        public List<CardConfigItem> cardList;

        public List<CardConfigItem> GetCardOfLevel(int level)
        {
            return cardList.Where(config => config.level == level).ToList();
        }
    }

    [Serializable]
    public class CardConfigItem
    {
        [Header("卡牌等级(从0开始)")]
        public int level;
        [Header("roll出权重")]
        public int weight;
        [Header("卡牌名字")]
        public string name;
        [Header("效果列表")]
        public List<EffectConfig> effects;
        [Header("价格")]
        public int price;

        public string GetEffectString()
        {
            string ret = "";
            for(int i = 0; i < effects.Count; i++)
            {
                var effect = effects[i];
                ret += effect.GetEffectDesc();
                if(i != effects.Count - 1)
                {
                    ret += "\n";
                }
            }
            return ret;
        }
    }

    [Serializable]
    public class EffectConfig
    {
        [Header("效果类型")]
        public EffectType effectType;
        [Header("效果值")]
        public float value;
        [Header("效果描述 可以使用{0} 代表数值")]
        public string effectDesc;
        public string GetEffectDesc()
        {
            return string.Format(effectDesc, value);
        }
    }
}

