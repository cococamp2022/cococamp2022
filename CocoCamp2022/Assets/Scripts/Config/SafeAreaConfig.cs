using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Config
{
    [CreateAssetMenu(fileName = "SafeAreaConfig", menuName = "CreateConfig/SafeAreaConfig")]
    [Serializable]
    public class SafeAreaConfig : ScriptableObject
    {
        [Header("威胁值降低的倍率")]
        public float reduceThreatSpeedScale;
        [Header("当安全区被发现后  多久能再恢复安全区")]
        public float recoverTime = 10;
        [Header("当到达恢复时间后   但是不满足恢复条件时  多久检测一次恢复")]
        public float checkRecoverTimer = 5;
        [Header("当安全区被发现后 消失动画的时间")]
        public float removeTime = 0.5f;
        [Header("当安全区被发现后 消失的缩放")]
        public float removeScale = 1.5f;
        [Header("当安全区被发现后 消失动画的曲线")]
        public DG.Tweening.Ease ease;

        [Header("淡出时间   应该都要统一")]
        public float fadeDuration = 0.2f;
        [Header("玩家在下面时  淡出后的颜色  包括透明度可调   应该都要统一")]
        public Color fadeColor;
    }
}

