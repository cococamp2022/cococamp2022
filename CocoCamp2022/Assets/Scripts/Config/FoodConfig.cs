using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Config
{
    [CreateAssetMenu(fileName = "FoodConfig", menuName = "CreateConfig/FoodConfig")]
    [Serializable]
    public class FoodConfig : ScriptableObject
    {
        [Header("食物的总量")]
        public int foodTotalAmount;
        [Header("进食速度的倍率")]
        public float eatSpeedScale = 1.0f;

        [Header("进入范围后的颜色")]
        public Color fadeColor;
        [Header("进入范围后的颜色变化的时间")]
        public float fadeDuration = 0.2f;
    }
}

