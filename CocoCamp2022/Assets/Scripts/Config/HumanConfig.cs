using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Logic;

namespace Config
{
    [CreateAssetMenu(fileName = "HumanConfig", menuName = "CreateConfig/HumanConfig")]
    [Serializable]
    public class HumanConfig : ScriptableObject
    {
        [Header("发现玩家之前每roll一次的时间间隔(秒)")]
        public float tryFindPlayerInterval;
        [Header("触发玩家被发现的概率曲线  X轴:当前威胁程度(0~1)当达到attackThreat时为1  Y轴:发现概率(0~1)")]
        public AnimationCurve triggerFindCurve;
        [Header("不同等级的人类AI数值")]
        public HumanConfigLevel[] humanLevelConfig;
    }

    [Serializable]
    public class HumanConfigLevel
    {
        [Header("等级(从0开始)")]
        public int level;
        [Header("威胁值上限")]
        public int maxThreat;
        [Header("开始追击所需的威胁值")]
        public int attackThreat;
        [Header("脱离追击的威胁值")]
        public int safeThreat;
        [Header("蟑螂在外活动每秒钟增加的威胁值")]
        public float addThreatSpeed;
        [Header("安全区每秒减少的威胁值")]
        public float reduceThreatSpeed;
        [Header("自动升到下一个等级所需的时间(秒)")]
        public float upgradeTime;

        [Space]

        [Header("释放技能的时间间隔的最小值")]
        public float skillIntervalMin;
        [Header("释放技能的时间间隔的最大值")]
        public float skillIntervalMax;
        [Header("技能间隔倍率与威胁值的曲线 X轴 attackThreat~maxThreat的比率(0~1)  Y轴 时间间隔的倍率(越小时间间隔越短) ")]
        public AnimationCurve skillIntervalScaleCurve;
        [Header("当进入安全区后 触发手电筒的概率与威胁值曲线 X轴 attackThreat~maxThreat的比率(0~1) Y轴 触发手电筒概率(0~1)")]
        public AnimationCurve flashLightTriggerCurve;

        [Header("技能列表")]
        public List<SkillConfig> skillList;
        public HumanSkillType GetHumanSkillTypeByWeight()
        {
            float sum = 0;
            foreach(var config in skillList)
            {
                sum += config.weight;
            }
            var random = UnityEngine.Random.Range(0, sum);

            sum = 0;
            foreach(var config in skillList)
            {
                if(random >= sum && random < sum + config.weight)
                {
                    return config.type;
                }
                else
                {
                    sum += config.weight;
                }
            }
            return HumanSkillType.Shoe;
        }
    }

    [Serializable]
    public class SkillConfig
    {
        [Header("技能类型")]
        public HumanSkillType type;
        [Header("权重")]
        public float weight;
    }
}

