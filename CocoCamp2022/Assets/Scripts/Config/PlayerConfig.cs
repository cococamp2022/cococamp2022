using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Config
{
    [CreateAssetMenu(fileName = "PlayerConfig", menuName = "CreateConfig/PlayerConfig")]
    [Serializable]
    public class PlayerConfig : ScriptableObject
    {
        #region Control
        [Header("加速度")]
        public float acc;
        [Header("初始速度")]
        public float initVel;
        [Header("减速的加速度")]
        public float stopAcc;
        [Header("转向的速度")]
        public float rotateVel;
        [Header("获得的食物量与移动速度的关系  x轴是食物量  y是速度的衰减倍率")]
        public AnimationCurve scoreToMoveSpeed;
        public float GetMoveSpeedScaleBySore(int score)
        {
            return scoreToMoveSpeed.Evaluate(score);
        }

        [Header("初始冲刺存储数量")]
        public int initDashCount;
        [Header("冲刺次数恢复CD")]
        public float initDashCD;
        [Header("冲刺结束后无法再次使用冲刺的时间")]
        public float dashEndCD;
        [Header("冲刺速度曲线  x轴是冲刺时间  y轴是当前速度的倍率")]
        public AnimationCurve dashVelCurve;
        [HideInInspector]
        public float dashTime => dashVelCurve.keys[dashVelCurve.keys.Length - 1].time;
        public float GetDashSpeedScale(float dashTime)
        {
            return dashVelCurve.Evaluate(dashTime);
        }

        #endregion

        [Space]

        #region Value
        [Header("最大生命值")]
        public int maxHP;
        [Header("进食速度(每秒增加的食物量)")]
        public float eatSpeed;


        #endregion
    }
}

