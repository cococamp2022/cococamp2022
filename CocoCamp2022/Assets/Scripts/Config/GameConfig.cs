using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Config
{
    [CreateAssetMenu(fileName = "GameConfig", menuName = "CreateConfig/GameConfig")]
    [Serializable]
    public class GameConfig : ScriptableObject
    {
        [Header("胜利条件  吃够一定量的食物")]
        public int successFood;
    }
}

