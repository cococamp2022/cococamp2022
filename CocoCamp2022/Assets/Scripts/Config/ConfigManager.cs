using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Config
{
    public class ConfigManager : SingletonMonoBehaviourClass<ConfigManager>
    {
        public GameConfig gameConfig;
        public PlayerConfig playerConfig;
        public LevelConfig levelConfig;
        public HumanConfig humanConfig;
        public CardConfig cardConfig;
    }
}
