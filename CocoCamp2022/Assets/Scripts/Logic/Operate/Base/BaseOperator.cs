using UnityEngine;

namespace Logic
{
    public abstract class BaseOperator<T, U> : BaseOperator<T>
    {
        protected BaseOperator(BaseOperateMachine<T> machine) : base(machine)
        {
        }

        public abstract void Enter(U param);
        public sealed override void Enter()
        {
            Enter(default(U));
        }

        public abstract bool CanEnter(U param);
        public sealed override bool CanEnter()
        {
            return CanEnter(default(U));
        }
    }
    public abstract class BaseOperator<T>
    {
        public abstract T state { get; }
        private BaseOperateMachine<T> machine;
        public BaseOperator(BaseOperateMachine<T> machine)
        {
            this.machine = machine;
        }
        public abstract void Enter();
        public abstract void Exit();
        public abstract bool CanBreak(T newState);
        public abstract bool CanEnter();
        public abstract void Update();

        protected void ChangeState(T newState)
        {
            machine.ChangeState(newState);
        }

        protected void ChangeState<U>(T newState, U param)
        {
            machine.ChangeState<U>(newState, param);
        }

        public virtual void Destroy()
        {
            machine = null;
        }
    }
}