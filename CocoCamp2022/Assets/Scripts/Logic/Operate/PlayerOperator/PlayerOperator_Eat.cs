using UnityEngine;
using UnityEngine.SceneManagement;

namespace Logic
{
    public class PlayerOperator_Eat : PlayerOperatorBase
    {
        private float timer = 0;
        public PlayerOperator_Eat(BaseOperateMachine<PlayerOperatorState> machine, Player player) : base(machine, player)
        {
        }

        public override PlayerOperatorState state => PlayerOperatorState.Eat;

        public override bool CanBreak(PlayerOperatorState newState)
        {
            return true;
        }

        public override bool CanEnter()
        {
            return player.InEnvironment(PlayerEnvironment.Food);
        }

        public override void Enter()
        {
            timer = 0;
        }

        public override void Exit()
        {
            
        }

        public override void Update()
        {
            var buttonPressed = Input.InputManager.instance.GetInputByTag(player.tag).actions["Eat"].IsPressed();
            if(!buttonPressed || !player.InEnvironment(PlayerEnvironment.Food))
            {
                ChangeState(PlayerOperatorState.Idle);
            }
            else
            {
                timer += Time.fixedDeltaTime;
                float oneScoreNeedTime = 1.0f / player.eatSpeed;
                while(timer > oneScoreNeedTime)
                {
                    if(!player.InEnvironment(PlayerEnvironment.Food)) break;
                    player.AddScore(1);
                    var food = player.GetEnvironmentObj<Food>(PlayerEnvironment.Food);
                    food.ConsumeFood(player, 1);
                    timer -= oneScoreNeedTime;
                }
            }
        }
    }
}