using UnityEngine;

namespace Logic
{
    public enum PlayerOperatorState
    {
        Idle,
        Move,
        Eat,
        Dash,
        Dead,
        Success,
    }
    public class PlayerOperatorManager : BaseOperateMachine<PlayerOperatorState>
    {
        private Player player;
        public PlayerOperatorManager(Player player)
        {
            this.player = player;
        }
        protected override PlayerOperatorState initState => PlayerOperatorState.Idle;

        protected override void RegisterAllOperator()
        {
            RegisterOperator(PlayerOperatorState.Idle, new PlayerOperator_Idle(this, player));
            RegisterOperator(PlayerOperatorState.Eat, new PlayerOperator_Eat(this, player));
            RegisterOperator(PlayerOperatorState.Move, new PlayerOperator_Move(this, player));
            RegisterOperator(PlayerOperatorState.Dash, new PlayerOperator_Dash(this, player));
            RegisterOperator(PlayerOperatorState.Dead, new PlayerOperator_Dead(this, player));
            RegisterOperator(PlayerOperatorState.Success, new PlayerOperator_Success(this, player));
        }
    }
}