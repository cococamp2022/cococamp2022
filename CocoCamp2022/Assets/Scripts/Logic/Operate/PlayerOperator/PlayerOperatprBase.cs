using Input;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace Logic
{
    public abstract class PlayerOperatorBase : BaseOperator<PlayerOperatorState>
    {
        protected Player player;
        public PlayerOperatorBase(BaseOperateMachine<PlayerOperatorState> machine, Player player) : base(machine)
        {
            this.player = player;
        }

        public override void Enter()
        {
            InputManager.instance.playerInput1.onActionTriggered += OnInput;

        }

        public override void Exit()
        {
            InputManager.instance.playerInput1.onActionTriggered -= OnInput;
        }

        protected virtual void OnInput(InputAction.CallbackContext context) { }
    }
}