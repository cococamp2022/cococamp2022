using Input;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace Logic
{
    public class PlayerOperator_Success : PlayerOperatorBase
    {
        public PlayerOperator_Success(BaseOperateMachine<PlayerOperatorState> machine, Player player) : base(machine, player)
        {
        }

        public override PlayerOperatorState state => PlayerOperatorState.Success;

        public override bool CanBreak(PlayerOperatorState newState)
        {
            return false;
        }

        public override bool CanEnter()
        {
            return true;
        }

        public override void Enter()
        {
            base.Enter();
            player.vel = Vector2.zero;
        }

        public override void Update()
        {

        }
    }
}