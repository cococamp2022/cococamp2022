using Input;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace Logic
{
    public class PlayerOperator_Idle : PlayerOperatorBase
    {
        public PlayerOperator_Idle(BaseOperateMachine<PlayerOperatorState> machine, Player player) : base(machine, player)
        {
        }

        public override PlayerOperatorState state => PlayerOperatorState.Idle;

        public override bool CanBreak(PlayerOperatorState newState)
        {
            return true;
        }

        public override bool CanEnter()
        {
            return true;
        }

        public override void Enter()
        {
            base.Enter();
            Update();
            player.PlayAnim("Idle");
        }

        public override void Update()
        {
            var actions = Input.InputManager.instance.GetInputByTag(player.tag).actions;
            if (actions["Dash"].WasPressedThisFrame())
            {
                ChangeState(PlayerOperatorState.Dash);
            }
            else if (actions["Move"].ReadValue<Vector2>().sqrMagnitude > 0.00001f)
            {
                ChangeState(PlayerOperatorState.Move);
            }
            else if (actions["Eat"].IsPressed())
            {
                ChangeState(PlayerOperatorState.Eat);
            }
            else if (player.vel.sqrMagnitude > 0.0001f)
            {
                ChangeState(PlayerOperatorState.Move);
            }
        }
    }
}