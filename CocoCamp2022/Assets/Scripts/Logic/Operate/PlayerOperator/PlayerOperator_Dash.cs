using UnityEngine;

namespace Logic
{
    public class PlayerOperator_Dash : PlayerOperatorBase
    {
        private Vector2 dashDir;
        private float dashTimer;
        public PlayerOperator_Dash(BaseOperateMachine<PlayerOperatorState> machine, Player player) : base(machine, player)
        {
        }

        public override PlayerOperatorState state => PlayerOperatorState.Dash;

        public override bool CanBreak(PlayerOperatorState newState)
        {
            return true;
        }

        public override bool CanEnter()
        {
            return player.dashCurrentCount > 0 && !player.InDashEndCD();
        }

        public override void Enter()
        {
            player.UseDashCount();
            dashTimer = 0;
            var move = Input.InputManager.instance.GetInputByTag(player.tag).actions["Move"].ReadValue<Vector2>();
            if (move.sqrMagnitude > 0.00001f)
            {
                dashDir = move.normalized;
            }
            else if (player.vel.sqrMagnitude > 0.00001f)
            {
                dashDir = player.vel.normalized;
            }
            else
            {
                dashDir = StaticUtils.GetVector2ByRotateZ(player.GetRotateZ());
            }
            player.UpdateRotate(StaticUtils.GetZRotateByVector2(dashDir));
        }

        public override void Exit()
        {
            player.StartDashEndCD();
        }

        public override void Update()
        {
            dashTimer += Time.fixedDeltaTime;
            if (dashTimer > Config.ConfigManager.instance.playerConfig.dashTime)
            {
                ChangeState(PlayerOperatorState.Idle);
            }
            else
            {
                player.vel = dashDir * Config.ConfigManager.instance.playerConfig.GetDashSpeedScale(dashTimer) * player.moveSpeed;
            }
        }
    }
}