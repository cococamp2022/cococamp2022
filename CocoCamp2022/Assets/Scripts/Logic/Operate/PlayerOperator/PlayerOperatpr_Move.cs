using Config;
using Input;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace Logic
{
    public class PlayerOperator_Move : PlayerOperatorBase
    {
        public PlayerOperator_Move(BaseOperateMachine<PlayerOperatorState> machine, Player player) : base(machine, player)
        {
        }

        public override PlayerOperatorState state => PlayerOperatorState.Move;

        public override bool CanBreak(PlayerOperatorState newState)
        {
            return true;
        }

        public override bool CanEnter()
        {
            return true;
        }

        public override void Enter()
        {
            base.Enter();
            Update();
            player.PlayAnim("Walk");
        }

        public override void Exit()
        {
            base.Exit();
            player.PlayAnim("Idle");
        }

        public override void Update()
        {
            if(Input.InputManager.instance.GetInputByTag(player.tag).actions["Dash"].WasPerformedThisFrame())
            {
                ChangeState(PlayerOperatorState.Dash);
                return;
            }
            UpdateMove();
            UpdateRotate();
        }

        private void UpdateMove()
        {
            var acc = InputManager.instance.GetInputByTag(player.tag).actions["Move"].ReadValue<Vector2>();
            var vel = player.vel;
            var moveSpeed = player.moveSpeed;
            Vector2 delVel = Vector2.zero;
            if (acc.sqrMagnitude > 0.0001f)
            {
                delVel = ConfigManager.instance.playerConfig.acc * acc * Time.fixedDeltaTime;
                vel += delVel;
            }
            else if (vel.sqrMagnitude > 0.0001f)
            {
                var delVelValue = ConfigManager.instance.playerConfig.stopAcc * Time.fixedDeltaTime;
                if (delVelValue > vel.magnitude)
                {
                    delVelValue = vel.magnitude;
                }
                delVel = -vel.normalized * delVelValue;
                vel += delVel;
            }

            if (vel.magnitude > moveSpeed)
            {
                vel = vel.normalized * moveSpeed;
            }
            else if (vel.sqrMagnitude < 0.0001f)
            {
                vel = Vector2.zero;
            }

            player.acc = acc;
            player.vel = vel;

            if (vel == Vector2.zero)
            {
                if (acc == Vector2.zero)
                {
                    ChangeState(PlayerOperatorState.Idle);
                }
            }
        }

        private void UpdateRotate()
        {
            var vel = player.vel;
            if (vel.sqrMagnitude > 0.00001f)
            {
                player.UpdateRotate(StaticUtils.GetZRotateByVector2(vel));
            }
        }
    }
}