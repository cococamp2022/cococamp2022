using Input;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace Logic
{
    public class PlayerOperator_Dead : PlayerOperatorBase
    {
        public PlayerOperator_Dead(BaseOperateMachine<PlayerOperatorState> machine, Player player) : base(machine, player)
        {
        }

        public override PlayerOperatorState state => PlayerOperatorState.Dead;

        public override bool CanBreak(PlayerOperatorState newState)
        {
            return false;
        }

        public override bool CanEnter()
        {
            return true;
        }

        public override void Enter()
        {
            base.Enter();
            player.vel = Vector2.zero;
            player.PlayAnim("dead");
        }

        public override void Exit()
        {
            base.Exit();
            player.PlayAnim("Idle");
        }

        public override void Update()
        {

        }
    }
}