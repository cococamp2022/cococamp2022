namespace Logic
{
    public class GameOperator_Intro : BaseOperator<GameOperatorState>
    {
        public GameOperator_Intro(BaseOperateMachine<GameOperatorState> machine) : base(machine)
        {
        }

        public override GameOperatorState state => GameOperatorState.Intro;

        public override void Enter()
        {
            GameController.instance.ShowUI(UISort.IntroUI);
        }

        public override void Exit()
        {
            GameController.instance.DestroyUI(UISort.IntroUI);
        }

        public override bool CanBreak(GameOperatorState newState)
        {
            return true;
        }

        public override bool CanEnter()
        {
            return true;
        }

        public override void Update()
        {
            
        }
    }
}