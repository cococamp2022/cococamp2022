using UnityEngine.SceneManagement;

namespace Logic
{
    public class GameOperatorParam_Level
    {
        public string levelID;
    }
    public class GameOperator_Level : BaseOperator<GameOperatorState, GameOperatorParam_Level>
    {
        public GameOperator_Level(BaseOperateMachine<GameOperatorState> machine) : base(machine)
        {
        }

        public override GameOperatorState state => GameOperatorState.Level;

        public override bool CanBreak(GameOperatorState newState)
        {
            return true;
        }

        public override bool CanEnter(GameOperatorParam_Level param)
        {
            return true;
        }

        public override void Enter(GameOperatorParam_Level param)
        {
            SceneController.instance.LoadScene(param.levelID);
        }

        public override void Exit()
        {
            SceneController.instance.ReleaseScene();
        }

        public override void Update()
        {
            
        }
    }
}