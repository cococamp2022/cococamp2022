using UnityEngine;

namespace Logic
{
    public class HumanOperator_AttackShoe : HumanOperatorBase<Player>
    {
        private Player targetPlayer;
        private Shoe shoe;
        public HumanOperator_AttackShoe(BaseOperateMachine<HumanOperatorState> machine, Human human) : base(machine, human)
        {
        }

        public override HumanOperatorState state => HumanOperatorState.Attack_Shoe;

        public override bool CanEnter(Player player)
        {
            return !player.InSafePlace();
        }

        public override bool CanBreak(HumanOperatorState newState)
        {
            return shoe == null || shoe.IsFinished;
        }

        public override void Enter(Player player)
        {
            targetPlayer = player;
            shoe = new Shoe();
            shoe.Init();
            shoe.Show(new ShoeObjData(){
                player = player,
                pos = player.pos
            });
        }

        public override void Exit()
        {
            shoe.Destroy();
            shoe = null;
        }

        public override void Update()
        {
            shoe.Update();
            if(shoe.IsFinished)
            {
                ChangeState(HumanOperatorState.Follow);
            }
        }
    }
}