using UnityEngine;

namespace Logic
{
    public class HumanOperator_Follow : HumanOperatorBase
    {
        public float skillIntervalTimer;
        public HumanOperator_Follow(BaseOperateMachine<HumanOperatorState> machine, Human human) : base(machine, human)
        {
        }

        public override HumanOperatorState state => HumanOperatorState.Follow;

        public override bool CanEnter()
        {
            return true;
        }

        public override bool CanBreak(HumanOperatorState newState)
        {
            return true;
        }

        public override void Enter()
        {
            human.SetThreatAboveAttackThreat();
            skillIntervalTimer = human.GetCurrentSkillInterval();
        }

        public override void Exit()
        {

        }

        public override void Update()
        {
            if (SceneController.instance.AllPlayersDead())
            {
                ChangeState(HumanOperatorState.Win);
            }

            if (human.currentThreat <= human.safeThreat)
            {
                ChangeState(HumanOperatorState.Idle);
                return;
            }

            skillIntervalTimer -= Time.fixedDeltaTime;
            if (skillIntervalTimer < 0)
            {
                skillIntervalTimer = human.GetCurrentSkillInterval();
                TryTriggerSkill();
            }
        }

        private void TryTriggerSkill()
        {
            var player = human.TryGetPlayerInSight();
            if (player == null)
            {
                if (human.TryTriggerFlashLight())
                {
                    player = SceneController.instance.GetRandomPlayer();
                    DoSkill(HumanSkillType.Flashlight, player);
                }
            }
            else
            {
                var type = human.GetHumanSkillType();
                DoSkill(type, player);
            }
        }

        private void DoSkill(HumanSkillType skillType, Player player)
        {
            Debug.Log($"TriggerSkill {skillType}");
            switch (skillType)
            {
                case HumanSkillType.Shoe:
                    ChangeState(HumanOperatorState.Attack_Shoe, player);
                    break;
                case HumanSkillType.Poison:
                    ChangeState(HumanOperatorState.Attack_Poison, player);
                    break;
                case HumanSkillType.Flashlight:
                    ChangeState(HumanOperatorState.Attack_Flashlight, player);
                    break;
            }
        }
    }
}