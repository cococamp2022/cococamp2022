namespace Logic
{
    public enum HumanOperatorState
    {
        Idle,
        Patrol,
        Follow,
        Attack_Shoe,
        Attack_Poison,
        Attack_Flashlight,
        Win,
        Fail,
    }
    public class HumanOperatorManager : BaseOperateMachine<HumanOperatorState>
    {
        private Human human;
        public HumanOperatorManager(Human human)
        {
            this.human = human;
        }
        protected override HumanOperatorState initState => HumanOperatorState.Idle;

        protected override void RegisterAllOperator()
        {
            RegisterOperator(HumanOperatorState.Idle, new HumanOperator_Idle(this, human));
            RegisterOperator(HumanOperatorState.Patrol, new HumanOperator_Patrol(this, human));
            RegisterOperator(HumanOperatorState.Follow, new HumanOperator_Follow(this, human));
            RegisterOperator(HumanOperatorState.Attack_Shoe, new HumanOperator_AttackShoe(this, human));
            RegisterOperator(HumanOperatorState.Attack_Poison, new HumanOperator_AttackPoison(this, human));
            RegisterOperator(HumanOperatorState.Attack_Flashlight, new HumanOperator_AttackFlashlight(this, human));
            RegisterOperator(HumanOperatorState.Win, new HumanOperator_Win(this, human));
            RegisterOperator(HumanOperatorState.Fail, new HumanOperator_Fail(this, human));
        }
    }
}