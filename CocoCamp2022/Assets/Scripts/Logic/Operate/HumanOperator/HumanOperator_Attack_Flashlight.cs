using UnityEngine;

namespace Logic
{
    public class HumanOperator_AttackFlashlight : HumanOperatorBase<Player>
    {
        private Player targetPlayer;
        private FlashLight flashLight;
        public HumanOperator_AttackFlashlight(BaseOperateMachine<HumanOperatorState> machine, Human human) : base(machine, human)
        {
        }

        public override HumanOperatorState state => HumanOperatorState.Attack_Flashlight;

        public override bool CanEnter(Player player)
        {
            return player.InEnvironment(PlayerEnvironment.SafeArea);
        }

        public override bool CanBreak(HumanOperatorState newState)
        {
            return flashLight == null || flashLight.IsFinished;
        }

        public override void Enter(Player player)
        {
            targetPlayer = player;
            flashLight = new FlashLight();
            flashLight.Init();
            flashLight.Show(new FlashLightObjData()
            {
                player = player,
            });
        }

        public override void Exit()
        {
            flashLight.Destroy();
            flashLight = null;
        }

        public override void Update()
        {
            flashLight.Update();
            if (flashLight.IsFinished)
            {
                ChangeState(HumanOperatorState.Follow);
            }
        }
    }
}