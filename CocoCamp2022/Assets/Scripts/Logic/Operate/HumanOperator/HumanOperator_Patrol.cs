using UnityEngine;

namespace Logic
{
    public class HumanOperator_Patrol : HumanOperatorBase
    {
        public HumanOperator_Patrol(BaseOperateMachine<HumanOperatorState> machine, Human human) : base(machine, human)
        {
        }

        public override HumanOperatorState state => HumanOperatorState.Patrol;

        public override bool CanEnter()
        {
            return true;
        }

        public override bool CanBreak(HumanOperatorState newState)
        {
            return true;
        }

        public override void Enter()
        {

        }

        public override void Exit()
        {

        }

        public override void Update()
        {

        }
    }
}