using UnityEngine;

namespace Logic
{
    public class HumanOperator_Idle : HumanOperatorBase
    {
        private float rollFindTimer;
        public HumanOperator_Idle(BaseOperateMachine<HumanOperatorState> machine, Human human) : base(machine, human)
        {
        }

        public override HumanOperatorState state => HumanOperatorState.Idle;

        public override bool CanEnter()
        {
            return true;
        }

        public override bool CanBreak(HumanOperatorState newState)
        {
            return true;
        }

        public override void Enter()
        {
            rollFindTimer = 0;
        }

        public override void Exit()
        {

        }

        public override void Update()
        {
            if(SceneController.instance.AllPlayersDead())
            {
                ChangeState(HumanOperatorState.Win);
            }

            rollFindTimer += Time.fixedDeltaTime;
            if(rollFindTimer > Config.ConfigManager.instance.humanConfig.tryFindPlayerInterval)
            {
                if(TryRollFindPlayer())
                {
                    ChangeState(HumanOperatorState.Follow);
                }
                else
                {
                    rollFindTimer -= Config.ConfigManager.instance.humanConfig.tryFindPlayerInterval;
                }
            }
        }

        private bool TryRollFindPlayer()
        {
            float currentPercent = human.currentThreat / human.attackThreat;
            float chance = Config.ConfigManager.instance.humanConfig.triggerFindCurve.Evaluate(currentPercent);
            return Random.value < chance;
        }
    }
}