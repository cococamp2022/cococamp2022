namespace Logic
{
    public abstract class HumanOperatorBase<T> : BaseOperator<HumanOperatorState, T>
    {
        protected Human human;
        public HumanOperatorBase(BaseOperateMachine<HumanOperatorState> machine, Human human) : base(machine)
        {
            this.human = human;
        }
    }
    public abstract class HumanOperatorBase : BaseOperator<HumanOperatorState>
    {
        protected Human human;
        public HumanOperatorBase(BaseOperateMachine<HumanOperatorState> machine, Human human) : base(machine)
        {
            this.human = human;
        }
    }
}