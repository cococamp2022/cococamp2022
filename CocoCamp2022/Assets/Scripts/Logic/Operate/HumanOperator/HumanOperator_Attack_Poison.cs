using UnityEngine;

namespace Logic
{
    public class HumanOperator_AttackPoison : HumanOperatorBase<Player>
    {
        private Player targetPlayer;
        private PoisonHand poisonHand;
        public HumanOperator_AttackPoison(BaseOperateMachine<HumanOperatorState> machine, Human human) : base(machine, human)
        {
        }

        public override HumanOperatorState state => HumanOperatorState.Attack_Poison;

        public override bool CanEnter(Player player)
        {
            return !player.InSafePlace();
        }

        public override bool CanBreak(HumanOperatorState newState)
        {
            return poisonHand == null || poisonHand.IsFinished;
        }

        public override void Enter(Player player)
        {
            targetPlayer = player;
            poisonHand = new PoisonHand();
            poisonHand.Init();
            poisonHand.Show(new PoisonHandObjData()
            {
                player = player,
            });
        }

        public override void Exit()
        {
            poisonHand.Destroy();
            poisonHand = null;
        }

        public override void Update()
        {
            poisonHand.Update();
            if (poisonHand.IsFinished)
            {
                ChangeState(HumanOperatorState.Follow);
            }
        }
    }
}