using UnityEngine;

namespace Logic
{
    public class HumanOperator_Fail : HumanOperatorBase
    {
        public float skillIntervalTimer;
        public HumanOperator_Fail(BaseOperateMachine<HumanOperatorState> machine, Human human) : base(machine, human)
        {
        }

        public override HumanOperatorState state => HumanOperatorState.Fail;

        public override bool CanEnter()
        {
            return true;
        }

        public override bool CanBreak(HumanOperatorState newState)
        {
            return false;
        }

        public override void Enter()
        {
        }

        public override void Exit()
        {

        }

        public override void Update()
        {

        }
    }
}