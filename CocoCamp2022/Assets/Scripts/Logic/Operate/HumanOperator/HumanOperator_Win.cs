using UnityEngine;

namespace Logic
{
    public class HumanOperator_Win : HumanOperatorBase
    {
        public float skillIntervalTimer;
        public HumanOperator_Win(BaseOperateMachine<HumanOperatorState> machine, Human human) : base(machine, human)
        {
        }

        public override HumanOperatorState state => HumanOperatorState.Win;

        public override bool CanEnter()
        {
            return true;
        }

        public override bool CanBreak(HumanOperatorState newState)
        {
            return false;
        }

        public override void Enter()
        {
            GameController.instance.ShowUI(UISort.DeadUI);
        }

        public override void Exit()
        {

        }

        public override void Update()
        {

        }
    }
}