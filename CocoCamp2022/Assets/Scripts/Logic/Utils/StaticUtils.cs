using UnityEngine;
using Logic;
using System.Collections;
using System;
using System.Collections.Generic;

public static class StaticUtils
{
    /// <summary>
    /// 以(0,1)为0度  逆时针方向  计算z轴的角度(degree)
    /// </summary>
    /// <returns></returns>
    public static float GetZRotateByVector2(Vector2 vector)
    {
        if (vector.y > -0.001f && vector.y < 0.001f)
        {
            if (vector.x > 0)
            {
                return 270;
            }
            else if (vector.x < 0)
            {
                return 90;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            if (vector.y > 0)
            {
                var tan = -vector.x / vector.y;
                return Mathf.Rad2Deg * Mathf.Atan(tan);
            }
            else
            {
                var tan = vector.x / vector.y;
                var degree = 180 - (Mathf.Rad2Deg * Mathf.Atan(tan));
                if (degree < 0)
                {
                    degree += 360;
                }
                return degree;
            }
        }
    }

    public static Vector2 GetVector2ByRotateZ(float rotateZ)
    {
        rotateZ = Mathf.Deg2Rad * rotateZ;
        return new Vector2(-Mathf.Sin(rotateZ), Mathf.Cos(rotateZ)).normalized;
    }

    public static int SkillObjectLayer = 1 << LayerMask.NameToLayer("SkillObject");

    public static int GroundLayer = 1 << LayerMask.NameToLayer("Ground");

    public static int FlashLightLayer = 1 << LayerMask.NameToLayer("FlashLightRange");
    public static int SafeArea = 1 << LayerMask.NameToLayer("SafeArea");

    public static T RandomPick<T>(IEnumerable<T> list, Func<T, int> weightGetter)
    {
        float sum = 0;
        foreach (var item in list)
        {
            sum += weightGetter(item);
        }
        var random = UnityEngine.Random.Range(0, sum);

        sum = 0;
        foreach (var config in list)
        {
            if (random >= sum && random < sum + weightGetter(config))
            {
                return config;
            }
            else
            {
                sum += weightGetter(config);
            }
        }
        return default;
    }

    public static List<T> RandomPick<T>(List<T> list, int count, Func<T, int> weightGetter)
    {
        List<T> returnList = new List<T>();
        for (int i = 0; i < count; i++)
        {
            if(list.Count > 0)
            {
                var a = RandomPick(list, weightGetter);
                returnList.Add(a);
                list.Remove(a);
            }
        }
        return returnList;
    }
}

