using System;
using System.Collections.Generic;
using UnityEngine;

namespace Logic
{
    public static class EventManager
    {
        private static Dictionary<string, BaseEvent> eventDict = new Dictionary<string, BaseEvent>();
        public static T Get<T>() where T : BaseEvent, new()
        {
            string name = typeof(T).Name;
            if (eventDict.TryGetValue(name, out BaseEvent e))
            {
                return e as T;
            }
            else
            {
                T newEvent = new T();
                eventDict.Add(name, newEvent);
                return newEvent;
            }
        }
    }
}