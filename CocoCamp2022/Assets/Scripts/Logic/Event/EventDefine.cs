using UnityEngine;
using System;
using Config;
using Input;
namespace Logic
{
    namespace EventDefine
    {
        public class ModulesInitFinished : Event { };
        public class InputMove : Event<PlayerIndex, Vector2> { };
        public class PlayerValueChanged : Event<EventParam.PlayerStateChangedParam> { };
        public class SceneInitFinished : Event { };
        public class SafeAreaStateChanged : Event<SafeArea, bool> { };
        public class StoreRefreshEvent : Event { }
    }

    namespace EventParam
    {
        public class PlayerStateChangedParam
        {

        }
    }
}
