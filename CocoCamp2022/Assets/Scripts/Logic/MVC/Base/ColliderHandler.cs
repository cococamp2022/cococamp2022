using System;
using UnityEngine;

namespace Logic
{
    public class ColliderHandler : MonoBehaviour
    {
        public static ColliderHandler Get(GameObject go)
        {
            ColliderHandler handler = go.GetComponent<ColliderHandler>();
            if (handler == null) handler = go.AddComponent<ColliderHandler>();
            return handler;
        }
        public GameObject root;
        public event Action<GameObject, Collider2D> onTriggerEnter;
        public event Action<GameObject, Collider2D> onTriggerStay;
        public event Action<GameObject, Collider2D> onTriggerExit;
        private void OnTriggerEnter2D(Collider2D collider2D)
        {
            onTriggerEnter?.Invoke(gameObject, collider2D);
        }

        private void OnTriggerStay2D(Collider2D collider2D)
        {
            onTriggerStay?.Invoke(gameObject, collider2D);
        }

        private void OnTriggerExit2D(Collider2D collider2D)
        {
            onTriggerExit?.Invoke(gameObject, collider2D);
        }
    }
}