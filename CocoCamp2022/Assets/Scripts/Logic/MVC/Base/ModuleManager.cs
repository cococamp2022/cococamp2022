namespace Logic
{
    public class ModuleManager : SingletonClass<ModuleManager>
    {
        public void InitAllModules()
        {
            GameController.instance.Init();
            DebugController.instance.Init();
            TipsController.instance.Init();
            SceneController.instance.Init();
            StoreController.instance.Init();

            EventManager.Get<EventDefine.ModulesInitFinished>().Fire();
        }

        public void Update()
        {
            GameController.instance.Update();
            SceneController.instance.Update();
        }

        public void FixedUpdate()
        {
            GameController.instance.FixedUpdate();
            SceneController.instance.FixedUpdate();
            DebugController.instance.FixedUpdate();
        }

        public void DestroyAllModules()
        {
            GameController.instance.Destroy();
            DebugController.instance.Destroy();
            TipsController.instance.Destroy();
            SceneController.instance.Destroy();
            StoreController.instance.Destroy();
        }
    }
}