namespace Logic
{
    //序号越高  在UI里的层级越高
    public enum UISort
    {
        MainUI,
        IntroUI,
        SelectCharactorUI,
        FoodUI,
        PlayerStateUI,
        HumanStateUI,
        StoreUI,
        HUD,
        DeadUI,
        SuccessUI,
        GameUI,

        #region Tips
        TipsUI,
        #endregion
    }
}