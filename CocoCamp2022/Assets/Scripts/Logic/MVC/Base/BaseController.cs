using System.Collections.Generic;
namespace Logic
{
    public interface IController
    {
        void Init();
        void Destroy();
    }
    public abstract class BaseController<T, V> : BaseController<T> where T : new() where V : BaseModel, new()
    {
        public V Model;

        public override void Init()
        {
            Model = new V();
            Model.Init();
            base.Init();
        }

        public override void Destroy()
        {
            Model.Destroy();
            base.Destroy();
        }
    }
    public abstract class BaseController<T> : SingletonClass<T>, IController where T : new()
    {
        protected Dictionary<UISort, BaseUI> UIDict = new Dictionary<UISort, BaseUI>();
        public virtual void Init()
        {
            RegisterEvents();
            RegisterAllUI();
            OnInit();
        }

        protected virtual void OnInit() { }

        protected virtual void RegisterEvents() { }
        protected virtual void RegisterAllUI() { }

        protected void RegisterUI(UISort uiOrder, BaseUI ui)
        {
            UIDict.Add(uiOrder, ui);
        }

        public virtual void Destroy()
        {
            UnRegisterAllUI();
            UnRegisterEvents();
        }

        protected virtual void UnRegisterEvents() { }
        protected virtual void UnRegisterAllUI()
        {
            foreach (var pair in UIDict)
            {
                pair.Value.Hide();
            }
            UIDict.Clear();
        }

        public void ShowUI(UISort uiOrder, object param)
        {
            if (UIDict.TryGetValue(uiOrder, out var ui))
            {
                ui.Init();
                ui.Show(param);
            }
        }

        public void ShowUI(UISort uiOrder)
        {
            if (UIDict.TryGetValue(uiOrder, out var ui))
            {
                ui.Init();
                ui.Show();
            }
        }

        public void HideUI(UISort uiOrder)
        {
            if (UIDict.TryGetValue(uiOrder, out var ui))
            {
                ui.Hide();
            }
        }

        public void DestroyUI(UISort uiOrder)
        {
            if (UIDict.TryGetValue(uiOrder, out var ui))
            {
                ui.Destroy();
            }
        }

        public U GetUI<U>(UISort uiOrder) where U : BaseUI
        {
            if (UIDict.TryGetValue(uiOrder, out var ui))
            {
                return ui as U;
            }
            return null;
        }

        /// <summary>
        /// 包含OnUpdate和UI的Update
        /// </summary>
        public void Update()
        {
            OnUpdate();
            UpdateAllUI();
        }

        public virtual void OnUpdate()
        {
        }

        protected void UpdateAllUI()
        {
            foreach (var ui in UIDict.Values)
            {
                ui.Update();
            }
        }

        public void FixedUpdate()
        {
            OnFixedUpdate();
        }

        protected virtual void OnFixedUpdate()
        {

        }
    }
}