using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Logic
{
    public class DebugController : BaseController<DebugController, DebugModel>
    {
        protected override void OnInit()
        {

        }
        protected override void OnFixedUpdate()
        {
            if (Keyboard.current.f1Key.wasPressedThisFrame)
            {
                GameController.instance.ShowUI(UISort.SuccessUI);
            }

            if (Keyboard.current.f2Key.wasPressedThisFrame)
            {
                TipsController.instance.ShowTips("测试");
            }

            if (Keyboard.current.f3Key.wasPressedThisFrame)
            {
                var player = SceneController.instance.Player1;
                var targetPos = player.pos + StaticUtils.GetVector2ByRotateZ(player.GetRotateZ()) * 10;
                player.CheckGroundBorder(targetPos, out var borderPos);
            }

            if (Keyboard.current.f4Key.wasPressedThisFrame)
            {
                dict.Clear();
                var human = SceneController.instance.Human;
                for (int i = 0; i < 900; i++)
                {
                    var type = human.GetHumanSkillType();
                    if (!dict.ContainsKey(type))
                    {
                        dict.Add(type, 1);
                    }
                    else
                    {
                        dict[type]++;
                    }
                }


                var log = "";
                foreach (var item in dict)
                {
                    log += $"{item.Key}:{item.Value}    ";
                }
                Debug.Log(log);
            }

            if (Keyboard.current.f5Key.wasPressedThisFrame)
            {
                var human = SceneController.instance.Human;
                var player = human.TryGetPlayerInSight();
                if (player == null) return;

                var type = human.GetHumanSkillType();
                human.operatorManager.ChangeState(HumanOperatorState.Attack_Shoe, player);
            }

            if (Keyboard.current.f6Key.wasPressedThisFrame)
            {
                var human = SceneController.instance.Human;
                var player = human.TryGetPlayerInSight();
                if (player == null) return;

                human.operatorManager.ChangeState(HumanOperatorState.Attack_Poison, player);
            }

            if (Keyboard.current.f7Key.wasPressedThisFrame)
            {
                var player = SceneController.instance.Player1;
                var pos = FlashLight.FindPos(player);
                Debug.Log($"pos {pos} offset {pos - player.pos}");
            }

            if (Keyboard.current.f8Key.wasPressedThisFrame)
            {
                var human = SceneController.instance.Human;
                var player = SceneController.instance.GetRandomPlayer();;
                if (player == null) return;

                human.operatorManager.ChangeState(HumanOperatorState.Attack_Flashlight, player);
            }
        }

        private Dictionary<HumanSkillType, int> dict = new Dictionary<HumanSkillType, int>();
    }
}