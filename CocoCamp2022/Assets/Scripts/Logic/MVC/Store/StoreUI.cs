using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using DG.Tweening;

namespace Logic
{
    public class StoreUI : BaseUI<StoreUIComp, Player>
    {
        protected override UISort sort => UISort.StoreUI;

        protected override string prefabName => "StoreUI";
        protected override void RegisterEvents()
        {
            EventManager.Get<EventDefine.StoreRefreshEvent>().AddListener(Refresh);
        }

        protected override void UnRegisterEvents()
        {
            EventManager.Get<EventDefine.StoreRefreshEvent>().RemoveListener(Refresh);
        }


        protected override void OnClick(GameObject obj)
        {
            if (obj.name == "buy")
            {
                Debug.Log("点击购买");
                var card = obj.transform.parent.GetComponent<StoreCardComp>();
                StoreController.instance.Model.BuyCard(data, card.data);
            }
            else if (obj.name == "close")
            {
                Hide();
            }
        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnInit()
        {

        }

        protected override void OnShow()
        {
            if (SceneController.instance.realHaveTwoPlayer)
            {
                sceneObject.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 1);
                if (data.tag == "Player1")
                {
                    sceneObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(-480, 0);
                }
                else
                {
                    sceneObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(480, 0);
                }
            }
            else
            {
                sceneObject.GetComponent<RectTransform>().localScale = Vector3.one;
                sceneObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
            }

            Refresh();
        }

        private void Refresh()
        {
            var itemList = StoreController.instance.Model.currentItems;
            for (int i = 0; i < itemList.Count; i++)
            {
                var card = sceneComponent.cardList[i];
                card.gameObject.SetActiveOptimize(true);
                card.SetData(itemList[i]);
            }

            for (int i = itemList.Count; i < Config.ConfigManager.instance.cardConfig.cardCount; i++)
            {
                var card = sceneComponent.cardList[i];
                card.gameObject.SetActiveOptimize(false);
            }
            ResetAllClicks();
        }
    }
}