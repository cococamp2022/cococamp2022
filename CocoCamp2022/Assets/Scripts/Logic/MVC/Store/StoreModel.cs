using System.Collections.Generic;
using Config;
using UnityEngine;

namespace Logic
{
    public enum EffectType
    {
        MaxHP,
        AutoHeal,
        MoveSpeed,
        EatSpeed,
        DashCount,
        DashCD,
        ShopDiscount,
    }

    public class StoreCardItemData
    {
        public CardConfigItem config;
        public bool isSold = false;
    }

    public class StoreModel : BaseModel
    {
        public List<StoreCardItemData> currentItems;
        public float shopDiscount = 1.0f;

        public override void Destroy()
        {

        }

        public override void Init()
        {
            currentItems = new List<StoreCardItemData>();
        }

        public void RefreshItems()
        {
            currentItems.Clear();
            var level = SceneController.instance.Human.currentLevel;
            var list = ConfigManager.instance.cardConfig.GetCardOfLevel(level);
            var configList = StaticUtils.RandomPick<CardConfigItem>(list, ConfigManager.instance.cardConfig.cardCount, (config) => config.weight);
            foreach (var config in configList)
            {
                currentItems.Add(new StoreCardItemData()
                {
                    config = config,
                });
            }
        }

        public void BuyCard(Player player, StoreCardItemData item)
        {
            if (!currentItems.Contains(item))
            {
                Debug.LogError("你要买的东西  并没有数据");
                return;
            }
            if (player.score < item.config.price)
            {
                TipsController.instance.ShowTips("您没有足够的食物");
            }
            else
            {
                player.SpendScore((int)(item.config.price * shopDiscount));
                ApplyEffect(player, item.config);
                SceneController.instance.Human.Upgrade();
                RefreshItems();
                EventManager.Get<EventDefine.StoreRefreshEvent>().Fire();
            }
        }

        private void ApplyEffect(Player player, CardConfigItem config)
        {
            foreach (var effect in config.effects)
            {
                switch (effect.effectType)
                {
                    case EffectType.MaxHP:
                        player.maxHP += (int)effect.value;
                        break;
                    case EffectType.AutoHeal:
                        player.autoHeal += effect.value;
                        break;
                    case EffectType.EatSpeed:
                        player.rawEatSpeed += effect.value;
                        break;
                    case EffectType.MoveSpeed:
                        player.rawSpeed += effect.value;
                        break;
                    case EffectType.DashCount:
                        player.dashMaxCount += (int)effect.value;
                        break;
                    case EffectType.DashCD:
                        player.dashCurrentCD += effect.value;
                        break;
                    case EffectType.ShopDiscount:
                        shopDiscount = effect.value;
                        break;
                }
            }
        }
    }
}