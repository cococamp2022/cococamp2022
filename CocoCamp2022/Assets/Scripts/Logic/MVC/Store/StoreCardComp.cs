
using UnityEngine;
using UnityEngine.UI;
using Config;

namespace Logic
{
    public class StoreCardComp : MonoBehaviour
    {
        public Text cardName;
        public Text effect;
        public Text price;
        public StoreCardItemData data;

        public void SetData(StoreCardItemData data)
        {
            this.data = data;
            var config = data.config;
            cardName.text = config.name;
            effect.text = config.GetEffectString();
            price.text = $"消耗食物:{(int)(config.price * StoreController.instance.Model.shopDiscount)}";
        }
    }
}