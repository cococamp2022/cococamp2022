using System.Collections.Generic;
using Config;
using UnityEngine;
using System;

namespace Logic
{
    public class StoreController : BaseController<StoreController, StoreModel>
    {
        protected override void OnInit()
        {
            
        }
        protected override void RegisterAllUI()
        {
            //RegisterUI(UISort.StoreUI, new StoreUI());
        }

        protected override void RegisterEvents()
        {
            EventManager.Get<EventDefine.SceneInitFinished>().AddListener(OnSceneInitFinished);
        }

        protected override void UnRegisterEvents()
        {
            EventManager.Get<EventDefine.SceneInitFinished>().RemoveListener(OnSceneInitFinished);
        }

        private void OnSceneInitFinished()
        {
            Model.RefreshItems();
        }
    }
}