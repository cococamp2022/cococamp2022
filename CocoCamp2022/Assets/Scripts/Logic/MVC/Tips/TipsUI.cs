using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using DG.Tweening;

namespace Logic
{
    public class TipsUI : BaseUI<TipsUIComponent, string>
    {
        protected override UISort sort => UISort.TipsUI;

        protected override string prefabName => "TipsUI";

        private float startPos = 100;
        private float endPos = 230;
        private float moveDuration = 1.8f;
        private float scaleInDuration = 0.15f;
        private float alphaOutDuration = 0.3f;

        protected override void OnClick(GameObject obj)
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnInit()
        {

        }

        protected override void OnShow()
        {
            sceneComponent.tips.text = data;
            var rect = sceneObject.GetComponent<RectTransform>();
            var canvasGroup = sceneObject.GetComponent<CanvasGroup>();
            DOTween.Sequence()
            .Append(rect.DOAnchorPosY(endPos, moveDuration).SetEase(Ease.InOutSine).ChangeStartValue(new Vector2(rect.anchoredPosition.x, startPos)))
            .Join(rect.DOScale(1, scaleInDuration).SetEase(Ease.OutSine).ChangeStartValue(new Vector3(0.01f, 0.01f, 1)))
            .Insert(moveDuration - alphaOutDuration, canvasGroup.DOFade(0, alphaOutDuration).SetEase(Ease.InOutSine).ChangeStartValue(1.0f))
            .AppendCallback(() => { Destroy(); });
        }
    }
}