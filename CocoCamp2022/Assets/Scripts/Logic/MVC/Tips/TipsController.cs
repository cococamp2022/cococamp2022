using System.Collections.Generic;
using Config;
using UnityEngine;
using System;

namespace Logic
{
    public class TipsController : BaseController<TipsController>
    {
        protected override void OnInit()
        {
            
        }
        protected override void RegisterAllUI()
        {
            
        }
        public void ShowTips(string tip)
        {
            var tipsUI = new TipsUI();
            tipsUI.Init();
            tipsUI.Show(tip);
        }
    }
}