using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using Cameras;

namespace Logic
{
    public class SceneController : BaseController<SceneController>
    {
        public SceneObjectRegister sceneObjectRegister;
        public Player Player1;
        public Player Player2;
        public Human Human;
        private List<Ground> grounds;
        private List<SafeArea> safeAreas;
        private List<Food> foods;
        private List<Home> homes;
        private List<PoisonArea> poisonAreas;
        public string currentLevelID;
        private string loadingLevel;
        private bool isInit = false;
        public bool initWithTwoPlayer = false;
        public int initPlayerIndex = 1;
        public bool realHaveTwoPlayer = false;
        protected override void OnInit()
        {

        }

        protected override void RegisterAllUI()
        {
            RegisterUI(UISort.FoodUI, new FoodUI());
        }

        public Player GetPlayerByTag(string tag)
        {
            if (tag == "Player1")
            {
                return Player1;
            }
            else if (tag == "Player2")
            {
                return Player2;
            }
            return Player1;
        }

        public Player GetRandomPlayer()
        {
            if (Player2 == null)
            {
                return Player1;
            }
            else
            {
                return Random.value < 0.5f ? Player1 : Player2;
            }
        }

        public void LoadScene(string LevelID)
        {
            if (loadingLevel.IsFilled())
            {
                Debug.LogError($"正在加载场景{loadingLevel}中  无法加载{LevelID}");
                return;
            }

            if (isInit || currentLevelID.IsFilled())
            {
                Debug.LogError($"当前已在场景{currentLevelID}中  无法加载{LevelID}  请先Release之前的场景");
                return;
            }

            CoroutineManager.instance.TryStartCoroutine(I_LoadScene(LevelID));
        }

        private IEnumerator I_LoadScene(string levelID)
        {
            Debug.Log($"Start LoadScene {levelID}");
            loadingLevel = levelID;
            var op = SceneManager.LoadSceneAsync(levelID);
            while (!op.isDone)
            {
                yield return null;
            }
            loadingLevel = null;
            Debug.Log($"Finish LoadScene {levelID}");

            InitScene(levelID);
        }

        public void InitScene(string levelID)
        {
            Debug.Log("InitScene");
            sceneObjectRegister = Transform.FindObjectOfType<SceneObjectRegister>();
            if (sceneObjectRegister == null)
            {
                Debug.LogError("未找到 SceneObjectRegister   请添加");
                return;
            }

            realHaveTwoPlayer = initWithTwoPlayer && sceneObjectRegister.player2 != null;

            if (sceneObjectRegister.player2 != null)
            {

                if (initWithTwoPlayer)
                {
                    sceneObjectRegister.player1.SetActiveOptimize(true);
                    sceneObjectRegister.player1_2.SetActiveOptimize(false);
                    sceneObjectRegister.player2.SetActiveOptimize(true);

                    Player1 = new Player();
                    Player1.InitWithGameObject(sceneObjectRegister.player1);

                    Player2 = new Player();
                    Player2.InitWithGameObject(sceneObjectRegister.player2);
                }
                else
                {
                    sceneObjectRegister.player2.SetActiveOptimize(false);

                    var initObj = initPlayerIndex == 1 ? sceneObjectRegister.player1 : sceneObjectRegister.player1_2;
                    var hideObj = initPlayerIndex == 1 ? sceneObjectRegister.player1_2 : sceneObjectRegister.player1;
                    initObj.SetActiveOptimize(true);
                    hideObj.SetActiveOptimize(false);
                    Player1 = new Player();
                    Player1.InitWithGameObject(initObj);
                }
            }
            else
            {
                Player1 = new Player();
                Player1.InitWithGameObject(sceneObjectRegister.player1);
            }

            //初始化镜头
            if (realHaveTwoPlayer)
            {
                //双人镜头
                CameraManager.instance.camera1.SetSide(true);
                CameraManager.instance.camera2.gameObject.SetActiveOptimize(true);
                CameraManager.instance.camera2.SetConfiner(sceneObjectRegister.cameraRange);
                CameraManager.instance.camera2.SetSide(false);
            }
            else
            {
                //单人的镜头
                CameraManager.instance.camera1.SetConfiner(sceneObjectRegister.cameraRange);
                CameraManager.instance.camera1.SetNormal();
                CameraManager.instance.camera2.gameObject.SetActiveOptimize(false);
            }

            //初始化ground
            grounds = new List<Ground>();
            foreach (var groundComp in sceneObjectRegister.groundRoot.GetComponentsInChildren<GroundComponent>())
            {
                var ground = new Ground();
                ground.InitWithGameObject(groundComp.gameObject);
                grounds.Add(ground);
            }

            //初始化safeArea
            safeAreas = new List<SafeArea>();
            foreach (var comp in sceneObjectRegister.safeAreaRoot.GetComponentsInChildren<SafeAreaComponent>())
            {
                var safeArea = new SafeArea();
                safeArea.InitWithGameObject(comp.gameObject);
                safeAreas.Add(safeArea);
            }

            //初始化food
            foods = new List<Food>();
            foreach (var comp in sceneObjectRegister.foodRoot.GetComponentsInChildren<FoodComp>())
            {
                var food = new Food();
                food.InitWithGameObject(comp.gameObject);
                foods.Add(food);
            }

            //初始化home
            homes = new List<Home>();
            foreach (var comp in sceneObjectRegister.homeRoot.GetComponentsInChildren<HomeComp>())
            {
                var home = new Home();
                home.InitWithGameObject(comp.gameObject);
                homes.Add(home);
            }

            poisonAreas = new List<PoisonArea>();

            //初始化Human
            Human = new Human();
            Human.Init();

            EventManager.Get<EventDefine.SceneInitFinished>().Fire();

            isInit = true;
            currentLevelID = levelID;
        }

        public void ReleaseScene()
        {
            isInit = false;
            currentLevelID = null;
            sceneObjectRegister = null;

            Player1.Destroy();
            Player1 = null;
            if (Player2 != null)
            {
                Player2.Destroy();
                Player2 = null;
            }
            foreach (var obj in grounds)
            {
                obj.Destroy();
            }
            grounds = null;

            foreach (var obj in safeAreas)
            {
                obj.Destroy();
            }
            safeAreas = null;

            foreach (var obj in homes)
            {
                obj.Destroy();
            }
            homes = null;

            //更新毒区   因为可能有删除操作  所以要倒着遍历
            for (int i = poisonAreas.Count - 1; i >= 0; i--)
            {
                var poisonArea = poisonAreas[i];
                poisonArea.Destroy();
            }
            poisonAreas = null;

            Human.Destroy();
            Human = null;
        }

        protected override void OnFixedUpdate()
        {
            if (!isInit) return;

            //更新毒区   因为可能有删除操作  所以要倒着遍历
            for (int i = poisonAreas.Count - 1; i >= 0; i--)
            {
                var poisonArea = poisonAreas[i];
                poisonArea.Update();
            }

            //更新安全区
            foreach (var safeArea in safeAreas)
            {
                safeArea.Update();
            }

            //更新玩家
            Player1.FixedUpdate();
            if (Player2 != null)
            {
                Player2.FixedUpdate();
            }

            //更新人类
            Human.Update();
        }

        public void AddPoisonArea(PoisonArea poisonArea)
        {
            if (!poisonAreas.Contains(poisonArea)) poisonAreas.Add(poisonArea);
        }

        public void RemovePoisonArea(PoisonArea poisonArea)
        {
            poisonAreas.Remove(poisonArea);
        }

        public bool AllPlayersDead()
        {
            if (Player1.operatorManager.currentState != PlayerOperatorState.Dead) return false;
            if (Player2 != null && Player2.operatorManager.currentState != PlayerOperatorState.Dead) return false;
            return true;
        }
    }
}