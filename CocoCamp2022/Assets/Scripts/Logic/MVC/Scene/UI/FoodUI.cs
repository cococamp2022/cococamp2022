using UnityEngine;

namespace Logic
{
    public class FoodUI : BaseUI<FoodUIComp, Food>
    {
        protected override UISort sort => UISort.FoodUI;

        protected override string prefabName => "FoodUI";
        public Food m_food;

        protected override void OnClick(GameObject obj)
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnInit()
        {

        }

        protected override void OnShow()
        {
            m_food = data;
            Refresh();
        }

        protected override void OnUpdate()
        {
            Refresh();
        }

        private void Refresh()
        {
            sceneComponent.SetValue(m_food.foodCount, m_food.maxFoodCount);
        }
    }
}