using UnityEngine;
using UnityEngine.UI;
namespace Logic
{
    public class FoodUIComp:MonoBehaviour
    {
        public Slider slider;
        public Text countText;
        public void SetValue(int haveCount, int maxFoodCount)
        {
            slider.value = (float)haveCount / (float)maxFoodCount;
            countText.text = $"{haveCount}/{maxFoodCount}";
        }
    }
}