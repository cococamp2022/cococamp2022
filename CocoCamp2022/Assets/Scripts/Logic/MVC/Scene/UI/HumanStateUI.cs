using UnityEngine;

namespace Logic
{
    public class HumanStateUI : BaseUI<HumanStateUIComp, Human>
    {
        protected override UISort sort => UISort.HumanStateUI;

        protected override string prefabName => "HumanStateUI";

        protected override void OnClick(GameObject obj)
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnInit()
        {

        }

        protected override void OnShow()
        {

        }

        protected override void OnUpdate()
        {
            Refresh();
        }

        private void Refresh()
        {
            var human = data;
            string lastLog = "";
            lastLog += $"State:{human.operatorManager.currentState}\n";
            lastLog += $"Level:{human.currentLevel}\n";
            lastLog += $"autoUpgrade:{human.autoUpgradeTimer}/{human.upgradeNeedTime}\n";
            lastLog += $"threat:{human.currentThreat}/{human.attackThreat}/{human.maxThreat}\n";
            lastLog += $"AboveAttack:{human.AboveAttackThreat}\n";
            lastLog += $"playerInSight:{human.playersInSight.Count}\n";
            var skillTime = human.operatorManager.currentState == HumanOperatorState.Follow ? (human.operatorManager.currentOperator as HumanOperator_Follow).skillIntervalTimer : -1;
            lastLog += $"nextSkillTime:{skillTime}\n";
            sceneComponent.controlState.text = lastLog;
        }
    }
}