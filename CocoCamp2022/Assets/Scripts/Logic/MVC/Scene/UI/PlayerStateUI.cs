using UnityEngine;

namespace Logic
{
    public class PlayerStateUI : BaseUI<PlayerStateUIComp, Player>
    {
        protected override UISort sort => UISort.PlayerStateUI;

        protected override string prefabName => "PlayerStateUI";

        protected override void OnClick(GameObject obj)
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnInit()
        {

        }

        protected override void OnShow()
        {
            if (data.tag == "Player2")
            {
                var rectTransform = sceneObject.GetComponent<RectTransform>();
                rectTransform.pivot = new Vector2(1, 1);
                rectTransform.anchorMin = new Vector2(1, 1);
                rectTransform.anchorMax = new Vector2(1, 1);
            }
        }

        protected override void OnUpdate()
        {
            Refresh();
        }

        private void Refresh()
        {
            var player = data;
            string lastLog = "";
            lastLog += $"HP:{player.hp}/{player.maxHP}\n";
            lastLog += $"MoveSpeed:{player.moveSpeed}\n";
            lastLog += $"EatSpeed:{player.eatSpeed}\n";
            lastLog += $"Food:{player.score}\n";
            lastLog += $"OperatorState:{player.operatorManager.currentState}\n";
            lastLog += $"acc:{player.acc}/{player.acc.magnitude}\n";
            lastLog += $"vel:{player.vel}/{player.vel.magnitude}\n";
            lastLog += $"rotateZ:{player.GetRotateZ()}\n";
            lastLog += $"dashCount:{player.dashCurrentCount}/{player.dashMaxCount}\n";
            lastLog += $"dashCD:{player.dashCDTimer}/{player.dashCurrentCD}\n";
            lastLog += $"dashEndCD:{player.dashEndCDTimer}/{Config.ConfigManager.instance.playerConfig.dashEndCD}\n";
            lastLog += $"Safe:{player.InEnvironment(PlayerEnvironment.SafeArea)} Home:{player.InEnvironment(PlayerEnvironment.Home)} Poison:{player.InEnvironment(PlayerEnvironment.PoisonArea)}\n";
            float poisonDamage = player.GetEnvironmentObj<PoisonArea>(PlayerEnvironment.PoisonArea)?.damage ?? 0;
            lastLog += $"PoisonDamage:{poisonDamage}";
            sceneComponent.controlState.text = lastLog;
        }
    }
}