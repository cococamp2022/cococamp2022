using UnityEngine;
using Input;
using UnityEngine.InputSystem;
using Config;
using Cameras;

namespace Logic
{
    public class FlashLightObjData
    {
        public Player player;
    }


    public class FlashLight : BaseObject<FlashLightComp, FlashLightObjData>
    {
        private enum FlashLightState
        {
            FadeIn,
            ShowLight,
            Find,
            HideLight,
            FadeOut,
            Finish,


            Flash
        }
        private FlashLightState currentState;
        private Vector2 pos => new Vector2(sceneComponent.transform.position.x, sceneComponent.transform.position.y);
        public bool IsFinished => currentState == FlashLightState.Finish;
        private float timer;
        protected override string prefabName => "SkillObject/FlashLight";

        private float currentRotateZ;
        private float minRotateZ;
        private float maxRotateZ;
        private float rotateDir;

        protected override void OnClick(GameObject obj)
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnInit()
        {
            sceneComponent.Init();
        }

        protected override void OnShow()
        {
            var pos = FindPos(data.player);
            if (pos == Vector2.zero)
            {
                currentState = FlashLightState.Finish;
                Debug.Log("Have No Place To Create");
            }
            else
            {
                sceneObject.transform.position = pos;

                var dir = (data.player.pos - pos);
                currentRotateZ = StaticUtils.GetZRotateByVector2(dir);
                minRotateZ = currentRotateZ - sceneComponent.findDegree / 2.0f;
                maxRotateZ = currentRotateZ + sceneComponent.findDegree / 2.0f;
                rotateDir = Random.value < 0.5f ? 1 : -1;
                sceneComponent.SetRotateZ(currentRotateZ);



                timer = 0;
                currentState = FlashLightState.FadeIn;
                sceneComponent.FadeIn();
            }
        }

        /// <summary>
        /// 找一个合适的地方来放手电筒
        /// </summary>
        /// <returns></returns>
        public static Vector2 FindPos(Player player)
        {
            //找一个只有GroundLayer的  离玩家最近的地方
            float disInterval = 0.5f;
            float dis = 0;
            int maxCheckCount = 50;
            Vector2[] dirArray = new Vector2[4];
            for (int i = 0; i < maxCheckCount; i++)
            {
                dis += disInterval;
                dirArray[0] = new Vector2(Random.value, Random.value).normalized;
                dirArray[1] = -dirArray[0];
                dirArray[2] = new Vector2(dirArray[0].y, -dirArray[0].x);
                dirArray[3] = -dirArray[2];
                for (int j = 0; j < 4; j++)
                {
                    var checkPos = player.pos + dirArray[j] * dis;
                    var colliders = Physics2D.OverlapPointAll(checkPos);
                    //Debug.Log($"checkPos {checkPos} offset:{checkPos - player.pos} count:{colliders.Length}");
                    if (colliders.Length > 0)
                    {
                        bool haveFlashLightLayer = false;
                        bool haveSafeArea = false;
                        foreach (var collider in colliders)
                        {
                            //有默认Layer-default的地方就不能放  其他的都可以放
                            if (collider.gameObject.layer == LayerMask.NameToLayer("FlashLightRange"))
                            {
                                //Debug.Log("Have FlashLight", collider.gameObject);
                                haveFlashLightLayer = true;
                            }
                            else if (collider.gameObject.layer == LayerMask.NameToLayer("SafeArea"))
                            {
                                //Debug.Log("SafeArea", collider.gameObject);
                                haveSafeArea = true;
                                break;
                            }
                            else
                            {
                                //Debug.Log($"Other Layer {collider.gameObject.layer}", collider.gameObject);
                            }
                        }
                        if (haveFlashLightLayer && !haveSafeArea) return checkPos;
                    }
                }
            }

            return Vector2.zero;
        }

        protected override void OnUpdate()
        {
            timer += Time.fixedDeltaTime;
            if (currentState == FlashLightState.FadeIn)
            {
                if (timer >= sceneComponent.fadeInTime)
                {
                    sceneComponent.ShowLight();
                    ChangeState(FlashLightState.ShowLight);
                }
            }
            else if (currentState == FlashLightState.ShowLight)
            {
                if (timer >= sceneComponent.showLightTime)
                {
                    ChangeState(FlashLightState.Find);
                }
            }
            else if (currentState == FlashLightState.Find)
            {
                //时间到了  或者发现了玩家出来了  直接结束
                if (timer >= sceneComponent.findTime || SceneController.instance.Human.playersInSight.Count > 0)
                {
                    sceneComponent.HideLight();
                    ChangeState(FlashLightState.HideLight);
                }
                else
                {
                    //打到了直接切换状态
                    if (data.player.InRangeOfSkill(HumanSkillType.Flashlight))
                    {
                        RemoveSafeArea();
                        sceneComponent.Flash();
                        ChangeState(FlashLightState.Flash);
                    }
                    else
                    {
                        UpdateFind();
                    }
                }
            }
            else if (currentState == FlashLightState.Flash)
            {
                if (timer >= sceneComponent.GetFlashTime())
                {
                    sceneComponent.HideLight();
                    ChangeState(FlashLightState.HideLight);
                }
                else
                {
                    UpdateFollow();
                }
            }
            else if (currentState == FlashLightState.HideLight)
            {
                if (timer >= sceneComponent.hideLightTime)
                {
                    sceneComponent.FadeOut();
                    ChangeState(FlashLightState.FadeOut);
                }
            }
            else if (currentState == FlashLightState.FadeOut)
            {
                if (timer >= sceneComponent.GetFlashTime())
                {
                    ChangeState(FlashLightState.Finish);
                }
            }
        }

        private void ChangeState(FlashLightState state)
        {
            timer = 0;
            currentState = state;
            OnUpdate();
        }


        private void UpdateFind()
        {
            currentRotateZ += sceneComponent.rotateSpeed * Time.fixedDeltaTime * rotateDir;
            if (currentRotateZ < minRotateZ)
            {
                currentRotateZ = minRotateZ;
                rotateDir = -rotateDir;
            }
            else if (currentRotateZ > maxRotateZ)
            {
                currentRotateZ = maxRotateZ;
                rotateDir = -rotateDir;
            }
            sceneComponent.SetRotateZ(currentRotateZ);
        }

        private void UpdateFollow()
        {
            var targetRotateZ = StaticUtils.GetZRotateByVector2(data.player.pos - pos);
            currentRotateZ = Mathf.LerpAngle(currentRotateZ, targetRotateZ, sceneComponent.followSpeed * Time.fixedDeltaTime);
            sceneComponent.SetRotateZ(currentRotateZ);
        }

        private void RemoveSafeArea()
        {
            var safeArea = data.player.GetEnvironmentObj<SafeArea>(PlayerEnvironment.SafeArea);
            if(safeArea == null) 
            {
                Debug.LogError("被发现了  但是找不到player的SafeArea");
                return;
            }

            safeArea.RemoveSafeArea();
        }
    }
}