using UnityEngine;
using Input;
using UnityEngine.InputSystem;
using Config;
using Cameras;

namespace Logic
{
    public class PoisonHandObjData
    {
        public Player player;
    }


    public class PoisonHand : BaseObject<PoisonHandComp, PoisonHandObjData>
    {
        private enum PoisonHandState
        {
            Show,
            Follow,
            Hide,
            Finish,
        }
        private PoisonHandState currentState;
        public bool IsFinished => currentState == PoisonHandState.Finish;
        private float timer;
        private float moveDistance;
        protected override string prefabName => "SkillObject/PoisonHand";

        protected override void OnClick(GameObject obj)
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnInit()
        {
            sceneComponent.Init();
        }

        protected override void OnShow()
        {
            sceneObject.transform.position = data.player.pos;
            timer = 0;
            currentState = PoisonHandState.Show;
            //初始必定能涂4个颜色 防止玩家不动
            moveDistance = sceneComponent.createPoisonAreaDistance * 4;
            sceneComponent.Show();
        }
        protected override void OnUpdate()
        {
            timer += Time.fixedDeltaTime;
            if (currentState == PoisonHandState.Show)
            {
                if (timer >= sceneComponent.fadeInTime)
                {
                    ChangeState(PoisonHandState.Follow);
                }
            }
            else if (currentState == PoisonHandState.Follow)
            {
                //玩家跑到安全区直接中断
                if (data.player.InSafePlace() || timer >= sceneComponent.followTime)
                {
                    sceneComponent.Hide();
                    ChangeState(PoisonHandState.Hide);
                }
                else
                {
                    UpdateFollow();
                    UpdateCreatePoisonArea();
                }
            }
            else if (currentState == PoisonHandState.Hide)
            {
                if (timer >= sceneComponent.fadeOutTIme)
                {
                    ChangeState(PoisonHandState.Finish);
                }
            }
        }

        private void ChangeState(PoisonHandState state)
        {
            timer = 0;
            currentState = state;
            OnUpdate();
        }

        private void UpdateFollow()
        {
            var currentPos = (Vector2)sceneObject.transform.position;
            var targetPos = data.player.pos;
            if ((targetPos - currentPos).sqrMagnitude > 0.00001f)
            {
                var newPos = Vector2.Lerp(currentPos, targetPos, sceneComponent.followSpeed * Time.fixedDeltaTime);
                moveDistance += (newPos - currentPos).magnitude;
                sceneObject.transform.position = newPos;
            }
        }
        private void UpdateCreatePoisonArea()
        {
            if (moveDistance > sceneComponent.createPoisonAreaDistance)
            {
                moveDistance -= sceneComponent.createPoisonAreaDistance;
                var poisonArea = new PoisonArea();
                poisonArea.Init();
                poisonArea.Show(new PoisonAreaObjData()
                {
                    pos = sceneObject.transform.position,
                    scale = sceneComponent.sizeCurve.Evaluate(timer / sceneComponent.followTime)
                });
            }
        }
    }
}