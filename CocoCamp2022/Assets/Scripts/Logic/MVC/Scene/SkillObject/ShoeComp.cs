using UnityEngine;
using DG.Tweening;

namespace Logic
{
    public class ShoeComp : MonoBehaviour
    {
        public SpriteRenderer shadow;
        public SpriteRenderer shoe;

        [Header("伤害")]
        public int damage = 150;
        [Header("阴影透明度")]
        public float shadowAlpha = 0.3f;
        [Header("阴影出现时间")]
        public float shadowFadeInTime = 0.3f;
        [Header("鞋子出现时间")]
        public float shoeFadeInTime = 0.2f;
        [Header("鞋子出现时的缩放")]
        public float upScale = 1.5f;
        // [Header("鞋子跟随速度")]
        // public float followSpeed = 0.1f;
        [Header("鞋子抖动的幅度")]
        public float shakeVolume = 0.1f;
        [Header("鞋子抖动的速度")]
        public float shakeSpeed = 0.1f;

        [Header("阶段1  跟随")]
        public float followDuration = 2.0f;
        [Header("阶段2  停止  准备拍下")]
        public float stopDuration = 0.5f;
        [Header("阶段3  拍下  结束时结算伤害")]
        public float shoeDownDuration = 0.2f;
        [Header("阶段4  拍下后静止")]
        public float stayDuration = 0.2f;
        [Header("阶段5  小时")]
        public float hideDuration = 0.5f;

        public void Init()
        {
            shadow.SetAlpha(0);
            shoe.SetAlpha(0);
            shoe.transform.localScale = new Vector3(upScale, upScale, 1);
        }

        public void ShowShadow()
        {
            shadow.DOFade(shadowAlpha, shadowFadeInTime);
        }

        public void ShowShoe()
        {
            DOTween.Sequence()
            .Append(shoe.DOFade(1.0f, shoeFadeInTime))
            .Join(shoe.transform.DOScale(1, shoeDownDuration));
        }

        public void Hide()
        {
            DOTween.Sequence()
            .Append(shoe.DOFade(0.0f, hideDuration))
            .Join(shadow.DOFade(0.0f, hideDuration));
        }
    }
}