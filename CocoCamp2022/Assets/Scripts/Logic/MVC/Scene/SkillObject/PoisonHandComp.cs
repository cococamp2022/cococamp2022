using UnityEngine;
using DG.Tweening;

namespace Logic
{
    public class PoisonHandComp : MonoBehaviour
    {
        public SpriteRenderer leftHand;
        public SpriteRenderer rightHand;
        private bool isLeftHand = false;
        private SpriteRenderer hand => isLeftHand ? leftHand : rightHand;

        [Header("跟随速度")]
        public float followSpeed = 0.1f;
        [Header("创建毒区移动距离间隔")]
        public float createPoisonAreaDistance = 0.1f;
        [Header("创建的毒区大小与跟随时间的曲线 x轴(0~1) y轴大小倍率")]
        public AnimationCurve sizeCurve;
        
        [Header("阶段1  出现")]
        public float fadeInTime = 2.0f;
        [Header("阶段2  跟随")]
        public float followTime = 0.5f;
        [Header("阶段3  消失")]
        public float fadeOutTIme = 0.2f;

        public void Init()
        {
            isLeftHand = Random.value < 0.5f;
            leftHand.gameObject.SetActiveOptimize(isLeftHand);
            rightHand.gameObject.SetActiveOptimize(!isLeftHand);
            hand.SetAlpha(0);
        }

        public void Show()
        {
            hand.DOFade(1.0f, fadeInTime);
        }

        public void Hide()
        {
            hand.DOFade(0.0f, fadeOutTIme);
        }
    }
}