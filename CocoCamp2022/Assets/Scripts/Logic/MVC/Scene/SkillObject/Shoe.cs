using UnityEngine;
using Input;
using UnityEngine.InputSystem;
using Config;
using Cameras;

namespace Logic
{
    public class ShoeObjData
    {
        public Player player;
        public Vector2 pos;
    }


    public class Shoe : BaseObject<ShoeComp, ShoeObjData>
    {
        private enum ShoeState
        {
            Follow,
            ReadyAttack,
            Attack,
            Stop,
            Hide,
            Finish,
        }
        private ShoeState currentState;
        public bool IsFinished => currentState == ShoeState.Finish;
        private float timer;
        private Vector4 perlinNoisePos;
        private Vector4 perlinNoiseDir;
        public float reduceThreatSpeedScale => 100;
        protected override string prefabName => "SkillObject/Shoe";

        protected override void OnClick(GameObject obj)
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnInit()
        {
            sceneComponent.Init();
        }

        protected override void OnShow()
        {
            sceneObject.transform.position = data.pos;
            timer = 0;
            currentState = ShoeState.Follow;
            perlinNoisePos = new Vector4(Random.value, Random.value, Random.value, Random.value);
            var dir1 = new Vector2(Random.value, Random.value).normalized;
            var dir2 = new Vector2(Random.value, Random.value).normalized;
            perlinNoiseDir = new Vector4(dir1.x, dir1.y, dir2.x, dir2.y);
            sceneComponent.ShowShadow();
        }
        protected override void OnUpdate()
        {
            timer += Time.fixedDeltaTime;
            if (currentState == ShoeState.Follow)
            {
                if (timer >= sceneComponent.followDuration)
                {
                    //准备攻击时 如果在安全区  直接取消攻击
                    if (data.player.InSafePlace())
                    {
                        sceneComponent.Hide();
                        ChangeState(ShoeState.Hide);
                    }
                    else
                    {
                        ChangeState(ShoeState.ReadyAttack);
                    }
                }
                else
                {
                    UpdateShake();
                    //UpdateFollow();
                }
            }
            else if (currentState == ShoeState.ReadyAttack)
            {
                if (timer >= sceneComponent.stopDuration)
                {
                    sceneComponent.ShowShoe();
                    ChangeState(ShoeState.Attack);
                }
            }
            else if (currentState == ShoeState.Attack)
            {
                if (timer >= sceneComponent.shoeDownDuration)
                {
                    CalculateDamage();
                    ChangeState(ShoeState.Stop);
                }
            }
            else if (currentState == ShoeState.Stop)
            {
                if (timer >= sceneComponent.stayDuration)
                {
                    sceneComponent.Hide();
                    ChangeState(ShoeState.Hide);
                }
            }
            else if (currentState == ShoeState.Hide)
            {
                if (timer >= sceneComponent.hideDuration)
                {
                    ChangeState(ShoeState.Finish);
                }
            }
        }

        private void ChangeState(ShoeState state)
        {
            timer = 0;
            currentState = state;
            OnUpdate();
        }

        // private void UpdateFollow()
        // {
        //     var currentPos = (Vector2)sceneObject.transform.position;
        //     var targetPos = data.player.pos;
        //     if ((targetPos - currentPos).sqrMagnitude > 0.00001f)
        //     {
        //         var newPos = Vector2.Lerp(currentPos, targetPos, sceneComponent.followSpeed * Time.fixedDeltaTime);
        //         sceneObject.transform.position = newPos;
        //     }
        // }

        private void UpdateShake()
        {
            perlinNoisePos += perlinNoiseDir * sceneComponent.shakeSpeed * Time.fixedDeltaTime;
            var offset = new Vector2(Mathf.PerlinNoise(perlinNoisePos.x, perlinNoisePos.y), Mathf.PerlinNoise(perlinNoisePos.z, perlinNoisePos.w));
            offset -= new Vector2(0.5f, 0.5f);
            offset *= sceneComponent.shakeVolume;
            sceneObject.transform.position = data.player.pos + offset;
        }

        private void CalculateDamage()
        {
            if (data.player.InRangeOfSkill(HumanSkillType.Shoe) && !data.player.InSafePlace())
            {
                data.player.Damage(sceneComponent.damage);
            }
        }
    }
}