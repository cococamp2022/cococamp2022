using UnityEngine;
using DG.Tweening;
using UnityEngine.U2D;

namespace Logic
{
    public class FlashLightComp : MonoBehaviour
    {
        public SpriteRenderer lightSprite;
        public SpriteRenderer flashLight;
        [Header("扫射时的角度范围")]
        public float findDegree = 60;
        [Header("扫射的速度")]
        public float rotateSpeed = 2;

        [Header("被发现时灯光闪烁时间间隔")]
        public float flashInterval = 0.15f;
        [Header("被发现时灯光闪烁次数")]
        public float flashCount = 2;
        [Header("被发现后灯光跟随的速度")]
        public float followSpeed = 10f;
        

        [Header("阶段1 淡入时间")]
        public float fadeInTime = 0.5f;
        [Header("阶段2 亮灯时间")]
        public float showLightTime = 0.5f;
        [Header("阶段3 寻找时间")]
        public float findTime = 5;
        [Header("阶段4 灭灯时间")]
        public float hideLightTime = 0.5f;
        [Header("阶段5 淡出时间")]
        public float fadeOutTime = 0.5f;

        public void Init()
        {
            lightSprite.SetAlpha(0);
            flashLight.SetAlpha(0);
        }

        public void FadeIn()
        {
            flashLight.DOFade(1, fadeInTime);
        }

        public void FadeOut()
        {
            flashLight.DOFade(0, fadeOutTime);
        }

        public void ShowLight()
        {
            lightSprite.SetAlpha(1);
        }

        public void HideLight()
        {
            lightSprite.SetAlpha(0);
        }

        public void Flash()
        {
            var seq = DOTween.Sequence();
            for (int i = 0; i < flashCount; i++)
            {
                seq
                .AppendCallback(() => { lightSprite.SetAlpha(0); })
                .AppendInterval(flashInterval)
                .AppendCallback(() => { lightSprite.SetAlpha(1); })
                .AppendInterval(flashInterval);
            }
        }

        public float GetFlashTime()
        {
            return flashInterval * 2 * flashCount + 0.2f;
        }

        public void SetRotateZ(float rotateZ)
        {
            transform.localRotation = Quaternion.Euler(0, 0, rotateZ);
        }
    }
}