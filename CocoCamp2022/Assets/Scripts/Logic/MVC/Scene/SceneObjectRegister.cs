using UnityEngine;
namespace Logic

{
    public class SceneObjectRegister:MonoBehaviour
    {
        public GameObject player1;
        public GameObject player1_2;
        public GameObject player2;
        public GameObject homeRoot;
        public GameObject groundRoot;
        public GameObject safeAreaRoot;
        public GameObject foodRoot;
        public PolygonCollider2D cameraRange;
    }
}