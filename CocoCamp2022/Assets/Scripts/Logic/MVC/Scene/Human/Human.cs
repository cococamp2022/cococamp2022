using Config;
using UnityEngine;
using System.Collections.Generic;

namespace Logic
{
    public enum HumanSkillType
    {
        Shoe,
        Poison,
        Flashlight,
    }
    public class Human
    {
        public HumanOperatorManager operatorManager;
        public List<Player> playersInSight;
        private HumanStateUI ui;
        public void Init()
        {
            playersInSight = new List<Player>();
            InitValue();
            operatorManager = new HumanOperatorManager(this);
            operatorManager.Init();
            ShowHumanUI();
        }

        public void Update()
        {
            UpdateValue();
            operatorManager.Update();
            ui.Update();
        }

        public void Destroy()
        {
            operatorManager.Destroy();
            operatorManager = null;
            HideHumanUI();
        }

        private void ShowHumanUI()
        {
            ui = new HumanStateUI();
            ui.Init();
            ui.Show(this);
        }

        private void HideHumanUI()
        {
            ui.Destroy();
            ui = null;
        }

        #region Value
        public int currentLevel;

        public int maxThreat;
        public int attackThreat;
        public int safeThreat;
        public int currentThreat = 0;
        public bool AboveAttackThreat => currentThreat > attackThreat;
        public float addThreatSpeed;
        public float reduceThreatSpeed;

        public float upgradeNeedTime;
        public float autoUpgradeTimer;

        private void InitValue()
        {
            UpdateBaseValue(currentLevel);
        }

        private void UpdateBaseValue(int level)
        {
            bool oldAboveAttackThreat = currentThreat > 0 && currentThreat >= attackThreat;

            currentLevel = level;
            var config = ConfigManager.instance.humanConfig.humanLevelConfig[currentLevel];
            maxThreat = config.maxThreat;
            attackThreat = config.attackThreat;
            safeThreat = config.safeThreat;
            currentThreat = oldAboveAttackThreat ? Mathf.Max(currentThreat, attackThreat) : currentThreat;
            upgradeNeedTime = config.upgradeTime;
            autoUpgradeTimer = 0;

            addThreatSpeed = config.addThreatSpeed;
            reduceThreatSpeed = config.reduceThreatSpeed;
        }

        private void UpdateValue()
        {
            UpdateAutoUpgradeTimer();
        }

        private void UpdateAutoUpgradeTimer()
        {
            autoUpgradeTimer += Time.fixedDeltaTime;
            if (autoUpgradeTimer > upgradeNeedTime)
            {
                autoUpgradeTimer = 0;
                Upgrade();
            }
        }

        public void Upgrade()
        {
            if (currentLevel < ConfigManager.instance.humanConfig.humanLevelConfig.Length - 1)
            {
                UpdateBaseValue(currentLevel + 1);
            }
        }

        public void ChangeThreat(int value)
        {
            currentThreat += value;
            currentThreat = Mathf.Clamp(currentThreat, 0, maxThreat);
        }

        public void SetThreatAboveAttackThreat()
        {
            if (currentThreat < attackThreat)
            {
                currentThreat = attackThreat;
            }
        }

        public float GetCurrentSkillInterval()
        {
            var config = ConfigManager.instance.humanConfig.humanLevelConfig[currentLevel];
            var interval = Random.Range(config.skillIntervalMin, config.skillIntervalMax);
            var progress = (float)(currentThreat - attackThreat) / (float)(maxThreat - attackThreat);
            var scale = config.skillIntervalScaleCurve.Evaluate(progress);
            return interval * scale;
        }

        public HumanSkillType GetHumanSkillType()
        {
            var config = ConfigManager.instance.humanConfig.humanLevelConfig[currentLevel];
            return config.GetHumanSkillTypeByWeight();
        }

        public bool TryTriggerFlashLight()
        {
            var config = ConfigManager.instance.humanConfig.humanLevelConfig[currentLevel];
            var progress = (float)(currentThreat - attackThreat) / (float)(maxThreat - attackThreat);
            return Random.value < config.flashLightTriggerCurve.Evaluate(progress);
        }

        public void AddPlayerInSight(Player player)
        {
            if (!playersInSight.Contains(player)) playersInSight.Add(player);
        }

        public void RemovePlayerInSight(Player player)
        {
            playersInSight.Remove(player);
        }

        public Player TryGetPlayerInSight()
        {
            if (playersInSight.Count == 0) return null;

            return playersInSight[Random.Range(0, playersInSight.Count)];
        }

        #endregion
    }
}