using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;

namespace Logic
{
    public class FoodComp : MonoBehaviour
    {
        public Config.FoodConfig config;
        public GameObject eatFlag;

        private List<Tween> fades;

        private bool inFade;
        public void FadeOutSprite()
        {
            if(inFade) return;
            if (fades == null)
            {
                fades = new List<Tween>();
                foreach (var renderer in GetComponentsInChildren<SpriteRenderer>())
                {
                    fades.Add(renderer.DOColor(config.fadeColor, config.fadeDuration).SetAutoKill(false));
                }
            }
            else
            {
                foreach (var fade in fades)
                {
                    fade.PlayForward();
                }
            }
            inFade = true;
        }

        public void FadeInSprite()
        {
            if(!inFade) return;
            foreach (var fade in fades)
            {
                fade.PlayBackwards();
            }
            inFade = false;
        }
    }
}