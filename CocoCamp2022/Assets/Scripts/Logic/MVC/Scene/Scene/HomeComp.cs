using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Logic
{
    public class HomeComp : MonoBehaviour
    {
        public float healSpeed = 20;
        private Dictionary<SpriteRenderer, SpriteTweenState> fadeDict = new Dictionary<SpriteRenderer, SpriteTweenState>();
        private bool inFade =false;
        public void FadeOutSprite(SpriteRenderer renderer)
        {
            if (fadeDict.TryGetValue(renderer, out var tweenState))
            {
                if (!tweenState.inFade)
                {
                    tweenState.tween.PlayForward();
                    tweenState.inFade = true;
                }
            }
            else
            {
                SpriteTweenState newTweenState = new SpriteTweenState
                (
                    renderer.DOColor(new Color(0.5f, 0.5f, 0.5f, 0.7f), 0.2f).SetAutoKill(false),
                    true
                );
                fadeDict.Add(renderer, newTweenState);
            }
        }

        public void FadeInSprite(SpriteRenderer renderer)
        {
            if (fadeDict.TryGetValue(renderer, out var tweenState) && tweenState.inFade)
            {
                tweenState.tween.PlayBackwards();
                tweenState.inFade = false;
            }
        }
    }
}