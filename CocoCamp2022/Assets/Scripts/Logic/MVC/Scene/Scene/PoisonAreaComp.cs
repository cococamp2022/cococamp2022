using UnityEngine;
using DG.Tweening;
using System.Collections.Generic;

namespace Logic
{
    public class PoisonAreaComp : MonoBehaviour
    {
        [Header("图片根节点  会对里面的所有sprite进行淡入淡出")]
        public GameObject spriteRoot;
        [Header("每秒造成的伤害")]
        public float damage;
        [Header("持续时间")]
        public float duration;
        [Header("伤害随着时间衰减曲线")]
        public AnimationCurve damageScaleCurve;
        [Header("淡入时间")]
        public float fadeInTime;
        [Header("淡出时间")]
        public float fadeOutTime;

        private List<Tweener> list;

        public void Init()
        {
            foreach (var renderer in spriteRoot.GetComponentsInChildren<SpriteRenderer>())
            {
                renderer.SetAlpha(0);
            }
        }
        public void Show()
        {
            if (list == null)
            {
                list = new List<Tweener>();
                foreach (var renderer in spriteRoot.GetComponentsInChildren<SpriteRenderer>())
                {
                    list.Add(renderer.DOFade(1, fadeInTime).SetAutoKill(false));
                }
            }
            else
            {
                foreach (var tween in list)
                {
                    tween.Rewind();
                    tween.PlayForward();
                }
            }
        }
        public void Hide()
        {
            foreach (var tween in list)
            {
                tween.PlayBackwards();
            }
            // foreach (var renderer in spriteRoot.GetComponentsInChildren<SpriteRenderer>())
            // {
            //     renderer.DOFade(0, fadeOutTime);
            // }
        }
    }
}