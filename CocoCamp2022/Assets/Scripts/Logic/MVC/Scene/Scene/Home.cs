using UnityEngine;
using Input;
using UnityEngine.InputSystem;
using Config;
using Cameras;
using System.Collections.Generic;

namespace Logic
{
    public class Home : BaseObject<HomeComp>
    {
        public float healSpeed => sceneComponent.healSpeed;
        public float reduceThreatSpeedScale => 100;
        protected override string prefabName => "Home";
        private List<Player> fadePlayers = new List<Player>();

        protected override void OnClick(GameObject obj)
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnInit()
        {

        }

        protected override void OnShow()
        {

        }

        protected override void OnTriggerEnter2D(GameObject obj, Collider2D collider2D)
        {
            var spriteRenderer = obj.GetComponent<SpriteRenderer>();
            var player = SceneController.instance.GetPlayerByTag(collider2D.tag);
            //有Sprite的是变半透的显示区域   没有sprite的是逻辑区域
            if (spriteRenderer == null)
            {
                player.SetEnvironment(PlayerEnvironment.Home, this, true);
                player.ShowShopUI();
            }
            else
            {
                if (!fadePlayers.Contains(player)) fadePlayers.Add(player);
                sceneComponent.FadeOutSprite(spriteRenderer);
            }
        }

        protected override void OnTriggerExit2D(GameObject obj, Collider2D collider2D)
        {
            var player = SceneController.instance.GetPlayerByTag(collider2D.tag);
            var spriteRenderer = obj.GetComponent<SpriteRenderer>();
            if (spriteRenderer == null)
            {
                player.SetEnvironment(PlayerEnvironment.Home, this, false);
                player.HideShopUI();
            }
            else
            {
                fadePlayers.Remove(player);
                if (fadePlayers.Count == 0)
                {
                    sceneComponent.FadeInSprite(spriteRenderer);
                }
            }
        }
    }
}