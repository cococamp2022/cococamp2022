using UnityEngine;
using Config;
using System.Collections.Generic;
using DG.Tweening;

namespace Logic
{
    public class SpriteTweenState
    {
        public SpriteTweenState(Tween tween, bool inFade)
        {
            this.tween = tween;
            this.inFade = inFade;
        }
        public Tween tween;
        public bool inFade;
    }
    public class SafeAreaComponent : MonoBehaviour
    {
        public SafeAreaConfig config;
        public GameObject colliderRoot;
        public GameObject spriteRoot;
        private Dictionary<SpriteRenderer, SpriteTweenState> fadeDict = new Dictionary<SpriteRenderer, SpriteTweenState>();
        private List<Tween> recoverList;
        private Tween recoverScaleTween;

        public void Init()
        {
            spriteRoot.SetActiveOptimize(true);
        }

        public void FadeOutSprite(SpriteRenderer renderer)
        {
            if (fadeDict.TryGetValue(renderer, out var tweenState))
            {
                if (!tweenState.inFade)
                {
                    tweenState.tween.PlayForward();
                    tweenState.inFade = true;
                }
            }
            else
            {
                SpriteTweenState newTweenState = new SpriteTweenState
                (
                    renderer.DOColor(config.fadeColor, config.fadeDuration).SetAutoKill(false),
                    true
                );
                fadeDict.Add(renderer, newTweenState);
            }
        }

        public void FadeInSprite(SpriteRenderer renderer)
        {
            if (fadeDict.TryGetValue(renderer, out var tweenState) && tweenState.inFade)
            {
                tweenState.tween.PlayBackwards();
                tweenState.inFade = false;
            }
        }

        public void SetFadeTweensToEnd()
        {
            foreach (var tween in fadeDict.Values)
            {
                tween.tween.Complete();
                tween.inFade = true;
            }
        }

        public void SetFadeTweensToStart()
        {
            foreach (var tween in fadeDict.Values)
            {
                tween.tween.Rewind();
                tween.inFade = false;
            }
        }

        public void Recover()
        {
            SetFadeTweensToStart();
            //所有普通Sprite用缓存的List
            foreach (var tween in recoverList)
            {
                tween.PlayBackwards();
            }

            foreach (var renderer in spriteRoot.GetComponentsInChildren<SpriteRenderer>())
            {
                //有半透功能的  单独处理
                if (renderer.gameObject.GetComponent<Collider2D>())
                {
                    renderer.DOColor(new Color(1, 1, 1, 1), config.removeTime).SetEase(Ease.OutCubic);
                }
            }

            recoverScaleTween.PlayBackwards();
        }

        public void Remove()
        {
            SetFadeTweensToEnd();

            if (recoverList == null)
            {
                recoverList = new List<Tween>();
                foreach (var renderer in spriteRoot.GetComponentsInChildren<SpriteRenderer>())
                {
                    //所有普通Sprite加入List
                    if (!renderer.gameObject.GetComponent<Collider2D>())
                    {
                        recoverList.Add(renderer.DOFade(0, config.removeTime).SetEase(Ease.InCubic).SetAutoKill(false));
                    }
                }
            }
            else
            {
                foreach (var tween in recoverList)
                {
                    tween.PlayForward();
                }
            }

            foreach (var renderer in spriteRoot.GetComponentsInChildren<SpriteRenderer>())
            {
                //有半透功能的  单独处理  因为他们的其实不是1  是半透的值
                if (renderer.gameObject.GetComponent<Collider2D>())
                {
                    renderer.DOFade(0, config.removeTime).SetEase(Ease.InCubic);
                }
            }

            if (recoverScaleTween == null)
            {
                recoverScaleTween = spriteRoot.transform.DOScale(config.removeScale, config.removeTime).SetEase(Ease.InCubic).SetAutoKill(false);
            }
            else
            {
                recoverScaleTween.PlayForward();
            }
        }
    }
}