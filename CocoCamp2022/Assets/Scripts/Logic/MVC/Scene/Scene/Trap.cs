using UnityEngine;
using Input;
using UnityEngine.InputSystem;
using Config;
using Cameras;

namespace Logic
{
    public class Trap : BaseObject<TrapComp>
    {
        protected override string prefabName => "Trap";

        protected override void OnClick(GameObject obj)
        {
            
        }

        protected override void OnDestroy()
        {
            
        }

        protected override void OnHide()
        {
            
        }

        protected override void OnInit()
        {
            
        }

        protected override void OnShow()
        {
            
        }
    }
}