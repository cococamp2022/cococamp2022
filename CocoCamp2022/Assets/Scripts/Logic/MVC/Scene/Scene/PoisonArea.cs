using UnityEngine;
using Input;
using UnityEngine.InputSystem;
using Config;
using Cameras;

namespace Logic
{
    public class PoisonAreaObjData
    {
        public Vector2 pos;
        public float scale;
    }
    public class PoisonArea : BaseObject<PoisonAreaComp, PoisonAreaObjData>
    {
        private float lifeTimer;
        public float damage
        {
            get
            {
                if (sceneComponent == null) return 0;
                return sceneComponent.damage * sceneComponent.damageScaleCurve.Evaluate(lifeTimer / sceneComponent.duration);
            }
        }

        protected override string prefabName => "SkillObject/PoisonArea";

        protected override void OnClick(GameObject obj)
        {

        }

        protected override void OnDestroy()
        {
            SceneController.instance.RemovePoisonArea(this);
        }

        protected override void OnHide()
        {

        }

        protected override void OnInit()
        {
            lifeTimer = 0;
            SceneController.instance.AddPoisonArea(this);
            sceneComponent.Init();
        }

        protected override void OnShow()
        {

            sceneObject.transform.position = data.pos;
            sceneObject.transform.localScale = new Vector3(data.scale, data.scale, 1);
            sceneComponent.Show();
        }

        protected override void OnUpdate()
        {
            lifeTimer += Time.fixedDeltaTime;
            if(lifeTimer > sceneComponent.duration - sceneComponent.fadeOutTime - 0.1f)
            {
                sceneComponent.Hide();
            }
            if (lifeTimer > sceneComponent.duration)
            {
                Destroy();
            }
        }

        protected override void OnTriggerEnter2D(GameObject obj,Collider2D collider2D)
        {
            var player = SceneController.instance.GetPlayerByTag(collider2D.tag);
            player.SetEnvironment(PlayerEnvironment.PoisonArea, this, true);
        }

        protected override void OnTriggerExit2D(GameObject obj,Collider2D collider2D)
        {
            var player = SceneController.instance.GetPlayerByTag(collider2D.tag);
            player.SetEnvironment(PlayerEnvironment.PoisonArea, this, false);
        }
    }
}