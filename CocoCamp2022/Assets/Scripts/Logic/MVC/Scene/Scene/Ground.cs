using UnityEngine;
using Input;
using UnityEngine.InputSystem;
using Config;
using Cameras;

namespace Logic
{
    public class Ground : BaseObject<GroundComponent>
    {
        protected override string prefabName => "Ground";

        protected override void OnClick(GameObject obj)
        {
            
        }

        protected override void OnDestroy()
        {
            
        }

        protected override void OnHide()
        {
            
        }

        protected override void OnInit()
        {
            
        }

        protected override void OnShow()
        {
            
        }
    }
}