using UnityEngine;
using Input;
using UnityEngine.InputSystem;
using Config;
using Cameras;
using System.Collections.Generic;

namespace Logic
{
    public class Food : BaseObject<FoodComp>
    {
        public int foodCount;
        public int maxFoodCount => sceneComponent.config?.foodTotalAmount ?? 0;
        public float eatSpeedScale => sceneComponent.config?.eatSpeedScale ?? 0;
        protected override string prefabName => "Food";

        private List<Player> players = new List<Player>();

        protected override void OnClick(GameObject obj)
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnInit()
        {
            foodCount = sceneComponent.config.foodTotalAmount;
        }

        protected override void OnShow()
        {

        }

        public void ConsumeFood(Player player, int value)
        {
            foodCount -= value;
            if (foodCount <= 0)
            {
                //player.SetEnvironment(PlayerEnvironment.Food, this, false);
                Destroy();
            }
        }

        protected override void OnTriggerEnter2D(GameObject obj, Collider2D collider2D)
        {
            //HighLight.Set(sceneObject, true);

            var player = SceneController.instance.GetPlayerByTag(collider2D.gameObject.tag);
            player.SetEnvironment(PlayerEnvironment.Food, this, true);

            //sceneComponent.eatFlag.SetActiveOptimize(true);
            
            if (!players.Contains(player)) players.Add(player);
            sceneComponent.FadeOutSprite();
        }

        protected override void OnTriggerExit2D(GameObject obj, Collider2D collider2D)
        {
            //HighLight.Set(sceneObject, false);

            var player = SceneController.instance.GetPlayerByTag(collider2D.gameObject.tag);
            player.SetEnvironment(PlayerEnvironment.Food, this, false);

            //sceneComponent.eatFlag.SetActiveOptimize(false);

            players.Remove(player);
            if (players.Count == 0) 
            sceneComponent.FadeInSprite();
        }
    }
}