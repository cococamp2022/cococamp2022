using UnityEngine;
using Input;
using UnityEngine.InputSystem;
using Config;
using Cameras;
using System.Collections.Generic;

namespace Logic
{
    public class SafeArea : BaseObject<SafeAreaComponent>
    {
        public float reduceThreatSpeedScale => sceneComponent?.config?.reduceThreatSpeedScale ?? 0;
        protected override string prefabName => "SafeArea";
        private List<Player> fadePlayers = new List<Player>();


        public bool isRemoved = false;
        private float recoverTimer = -1;

        protected override void OnClick(GameObject obj)
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnInit()
        {
            sceneComponent.Init();
            isRemoved = false;
            recoverTimer = -1;
        }

        protected override void OnShow()
        {

        }

        protected override void OnUpdate()
        {
            if (isRemoved)
            {
                if (recoverTimer > 0)
                {
                    recoverTimer -= Time.fixedDeltaTime;
                }
                if (recoverTimer < 0)
                {
                    TryRecover();
                }
            }
        }

        private void TryRecover()
        {
            if (isRemoved && !CheckHavePlayersInMyRange())
            {
                sceneComponent.Recover();
                isRemoved = false;
                recoverTimer = -1;
                EventManager.Get<EventDefine.SafeAreaStateChanged>().Fire(this, true);
            }
            else
            {
                recoverTimer = sceneComponent?.config?.checkRecoverTimer ?? 5;
            }
        }

        private bool CheckHavePlayersInMyRange()
        {
            foreach (var collider in sceneComponent.colliderRoot.GetComponentsInChildren<Collider2D>())
            {
                if (SceneController.instance.Player1.InRangeOfCollider(collider)) return true;
            }
            if (SceneController.instance.Player2 != null)
            {
                foreach (var collider in sceneComponent.colliderRoot.GetComponentsInChildren<Collider2D>())
                {
                    if (SceneController.instance.Player2.InRangeOfCollider(collider)) return true;
                }
            }
            return false;
        }

        public void RemoveSafeArea()
        {
            if (isRemoved) return;
            isRemoved = true;
            recoverTimer = sceneComponent?.config?.recoverTime ?? 10;
            sceneComponent.Remove();
            EventManager.Get<EventDefine.SafeAreaStateChanged>().Fire(this, false);
        }

        protected override void OnTriggerEnter2D(GameObject obj, Collider2D collider2D)
        {
            var spriteRenderer = obj.GetComponent<SpriteRenderer>();
            var player = SceneController.instance.GetPlayerByTag(collider2D.tag);
            //有Sprite的是变半透的显示区域   没有sprite的是逻辑区域
            if (spriteRenderer == null)
            {
                player.SetEnvironment(PlayerEnvironment.SafeArea, this, true);
            }
            else
            {
                if (!isRemoved)
                {
                    if (!fadePlayers.Contains(player)) fadePlayers.Add(player);
                    sceneComponent.FadeOutSprite(spriteRenderer);
                }
            }
        }

        protected override void OnTriggerExit2D(GameObject obj, Collider2D collider2D)
        {
            var player = SceneController.instance.GetPlayerByTag(collider2D.tag);
            var spriteRenderer = obj.GetComponent<SpriteRenderer>();
            if (spriteRenderer == null)
            {
                player.SetEnvironment(PlayerEnvironment.SafeArea, this, false);
            }
            else
            {
                if (!isRemoved)
                {
                    fadePlayers.Remove(player);
                    if (fadePlayers.Count == 0)
                    {
                        sceneComponent.FadeInSprite(spriteRenderer);
                    }
                }
            }
        }
    }
}