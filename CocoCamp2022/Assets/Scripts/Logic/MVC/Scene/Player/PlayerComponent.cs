using UnityEngine;

namespace Logic
{
    public class PlayerComponent : MonoBehaviour
    {
        public Player player;
        public Transform spriteRoot;
        public GameObject colliderRoot;

        public Animator animator;
        public SpriteRenderer threat;
    }
}