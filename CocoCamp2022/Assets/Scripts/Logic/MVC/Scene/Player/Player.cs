using UnityEngine;
using Input;
using UnityEngine.InputSystem;
using Config;
using Cameras;
using System.Collections.Generic;

namespace Logic
{
    public enum PlayerEnvironment
    {
        /// <summary>
        /// 安全区
        /// </summary>
        SafeArea,
        /// <summary>
        /// 毒区
        /// </summary>
        PoisonArea,
        /// <summary>
        /// 快乐老家
        /// </summary>
        Home,
        /// <summary>
        /// 食物
        /// </summary>
        Food,
    }
    public class Player : BaseObject<PlayerComponent>
    {
        public string tag => sceneObject.tag;
        protected override string prefabName => "Player";
        private PlayerStateUI ui;
        private HudUI hud;
        private MainUI mainUI;
        private StoreUI shopUI;

        protected override void RegisterEvents()
        {
            EventManager.Get<EventDefine.SceneInitFinished>().AddListener(OnSceneInitFinished);
            EventManager.Get<EventDefine.SafeAreaStateChanged>().AddListener(OnSafeAreaStateChanged);
        }

        protected override void UnRegisterEvents()
        {
            EventManager.Get<EventDefine.SceneInitFinished>().RemoveListener(OnSceneInitFinished);
            EventManager.Get<EventDefine.SafeAreaStateChanged>().RemoveListener(OnSafeAreaStateChanged);
        }

        private void OnSceneInitFinished()
        {
            InitEnvironment();
        }

        protected override void OnClick(GameObject obj)
        {

        }

        protected override void OnDestroy()
        {
            sceneComponent.player = null;
            operatorManager.Destroy();
            ReleaseEnvironment();
        }

        protected override void OnHide()
        {
            if (tag == "Player1")
            {
                CameraManager.instance.camera1.SetTarget(null);
            }
            else
            {
                CameraManager.instance.camera2.SetTarget(null);
            }
            HideStateUI();
        }

        protected override void OnInit()
        {
            sceneComponent.player = this;
            InitControl();
            InitValue();
        }

        protected override void OnShow()
        {
            ShowStateUI();
            if (tag == "Player1")
            {
                CameraManager.instance.camera1.SetTarget(sceneObject.transform);
            }
            else
            {
                CameraManager.instance.camera2.SetTarget(sceneObject.transform);
            }
        }

        private void ShowStateUI()
        {
            ui = new PlayerStateUI();
            ui.Init();
            ui.Show(this);

            hud = new HudUI();
            hud.Init();
            hud.Show(this);

            mainUI = new MainUI();
            mainUI.Init();
            mainUI.Show(this);

            shopUI = new StoreUI();
            shopUI.Init();
        }

        private void HideStateUI()
        {
            ui.Destroy();
            ui = null;

            hud.Destroy();
            hud = null;

            mainUI.Destroy();
            mainUI = null;

            shopUI.Destroy();
            shopUI = null;
        }

        public void ShowShopUI()
        {
            shopUI.Show(this);
        }

        public void HideShopUI()
        {
            shopUI.Hide();
        }

        public void FixedUpdate()
        {
            if (!isInit) return;

            UpdateValue();
            UpdateControl();
            ui.Update();
            hud.Update();
            mainUI.Update();
        }

        #region Environment        
        public PoisonArea currentPoisonArea;

        private Dictionary<PlayerEnvironment, List<BaseObject>> environmentObjectDict;

        private void InitEnvironment()
        {
            environmentObjectDict = new Dictionary<PlayerEnvironment, List<BaseObject>>();
            //初始情况下确实是在视野里的
            SceneController.instance.Human.AddPlayerInSight(this);
            //要等待所有的地形区域都加载完了  再把player的碰撞体加入场景中  才能正确触发碰撞体事件
            sceneComponent.colliderRoot.SetActiveOptimize(true);
        }

        private void ReleaseEnvironment()
        {
            environmentObjectDict = null;
        }

        public bool InEnvironment(PlayerEnvironment environment)
        {
            return GetEnvironmentObj(environment) != null;
        }

        /// <summary>
        /// 在安全区和老家
        /// </summary>
        public bool InSafePlace()
        {
            return InEnvironment(PlayerEnvironment.SafeArea) || InEnvironment(PlayerEnvironment.Home);
        }

        public BaseObject GetEnvironmentObj(PlayerEnvironment environment)
        {
            if (environmentObjectDict.TryGetValue(environment, out var list))
            {
                if (list.Count > 0)
                {
                    if (environment == PlayerEnvironment.SafeArea)
                    {
                        //安全区还要额外检测IsRemoved参数
                        for (int i = list.Count - 1; i >= 0; i--)
                        {
                            if (!(list[i] as SafeArea).isRemoved) return list[i];
                        }
                    }
                    else
                    {
                        return list[list.Count - 1];
                    }
                }
            }
            return null;
        }

        public T GetEnvironmentObj<T>(PlayerEnvironment environment) where T : BaseObject
        {
            return GetEnvironmentObj(environment) as T;
        }
        public void SetEnvironment(PlayerEnvironment environment, BaseObject obj, bool value)
        {
            List<BaseObject> list;
            if (!environmentObjectDict.TryGetValue(environment, out list))
            {
                list = new List<BaseObject>();
                environmentObjectDict.Add(environment, list);
            }
            if (value)
            {
                if (list.Contains(obj))
                {
                    Debug.LogError($"重复添加了 {environment}  物体:{obj.sceneObject.name}", obj.sceneObject);
                }
                else
                {
                    list.Add(obj);
                    OnPlayerEnvironmentChanged(environment, value);
                }
            }
            else
            {
                if (list.Remove(obj))
                {
                    OnPlayerEnvironmentChanged(environment, value);
                }
                else
                {
                    Debug.LogError($"删除 {environment} 物体:{obj.sceneObject.name} 失败 没有存储这个物体", obj.sceneObject);
                }
            }
        }

        private void OnSafeAreaStateChanged(SafeArea safeArea, bool isRecover)
        {
            OnPlayerEnvironmentChanged(PlayerEnvironment.SafeArea, isRecover);
        }

        private void OnPlayerEnvironmentChanged(PlayerEnvironment environment, bool value)
        {
            switch (environment)
            {
                case PlayerEnvironment.SafeArea:
                    if (InSafePlace())
                    {
                        SceneController.instance.Human.RemovePlayerInSight(this);
                    }
                    else
                    {
                        SceneController.instance.Human.AddPlayerInSight(this);
                    }
                    break;
                case PlayerEnvironment.Home:
                    if (InSafePlace())
                    {
                        SceneController.instance.Human.RemovePlayerInSight(this);
                    }
                    else
                    {
                        SceneController.instance.Human.AddPlayerInSight(this);
                    }
                    if (value)
                    {
                        StoreController.instance.ShowUI(UISort.StoreUI, this);
                    }
                    else
                    {
                        StoreController.instance.HideUI(UISort.StoreUI);
                    }
                    break;
                    // case PlayerEnvironment.Food:
                    //     var foodUI = SceneController.instance.GetUI<FoodUI>(UISort.FoodUI);
                    //     if (InEnvironment(PlayerEnvironment.Food))
                    //     {
                    //         var food = GetEnvironmentObj<Food>(PlayerEnvironment.Food);

                    //         if (!foodUI.isShow)
                    //         {
                    //             SceneController.instance.ShowUI(UISort.FoodUI, food);
                    //         }
                    //         else if (foodUI.m_food != food)
                    //         {
                    //             SceneController.instance.HideUI(UISort.FoodUI);
                    //             SceneController.instance.ShowUI(UISort.FoodUI, food);
                    //         }
                    //     }
                    //     else
                    //     {
                    //         if (foodUI.isShow)
                    //         {
                    //             SceneController.instance.HideUI(UISort.FoodUI);
                    //         }
                    //     }
                    //     break;
            }
        }

        #endregion

        #region Value
        /// <summary>
        /// 原始速度
        /// </summary>
        public float rawSpeed;
        /// <summary>
        /// 真实速度  被进食衰减后的速度
        /// </summary>
        /// <returns></returns>
        public float moveSpeed => ConfigManager.instance.playerConfig.GetMoveSpeedScaleBySore(score) * rawSpeed;
        public float rawEatSpeed;
        public float eatSpeed => rawEatSpeed * (GetEnvironmentObj<Food>(PlayerEnvironment.Food)?.eatSpeedScale ?? 0);
        public int hp;
        public int maxHP;
        public int score;
        public float autoHeal;

        private void InitValue()
        {
            rawSpeed = ConfigManager.instance.playerConfig.initVel;
            rawEatSpeed = ConfigManager.instance.playerConfig.eatSpeed;
            maxHP = ConfigManager.instance.playerConfig.maxHP;
            hp = maxHP;
            score = 0;
            autoHeal = 0;
        }

        private void UpdateValue()
        {
            UpdateSuccess();
            UpdateHeal();
            UpdateThreat();
            UpdatePoison();
            UpdateDead();
        }

        private void UpdateSuccess()
        {
            if (score >= ConfigManager.instance.gameConfig.successFood)
            {
                operatorManager.ChangeState(PlayerOperatorState.Success);
                SceneController.instance.Human.operatorManager.ChangeState(HumanOperatorState.Fail);
                GameController.instance.ShowUI(UISort.SuccessUI);
            }
        }

        private float autoHealTimer = 0;
        private void UpdateHeal()
        {
            float healSpeed = autoHeal;
            if (InEnvironment(PlayerEnvironment.Home))
            {
                healSpeed += GetEnvironmentObj<Home>(PlayerEnvironment.Home).healSpeed;
            }
            if (healSpeed > 0)
            {
                autoHealTimer += Time.fixedDeltaTime;
                float oneAutoHealTime = 1.0f / healSpeed;
                while (autoHealTimer > oneAutoHealTime)
                {
                    Heal(1);
                    autoHealTimer -= oneAutoHealTime;
                }
            }
        }

        private void UpdateDead()
        {
            if (hp <= 0)
            {
                operatorManager.ChangeState(PlayerOperatorState.Dead);
            }
        }

        private float threatTimer = 0;
        private void UpdateThreat()
        {
            int changeThreat = 0;
            float updateNeedTime = float.MaxValue;
            if (InEnvironment(PlayerEnvironment.SafeArea))
            {
                changeThreat = -1;
                updateNeedTime = 1.0f / (GetEnvironmentObj<SafeArea>(PlayerEnvironment.SafeArea).reduceThreatSpeedScale * SceneController.instance.Human.reduceThreatSpeed);
            }
            else if (InEnvironment(PlayerEnvironment.Home))
            {
                changeThreat = -1;
                updateNeedTime = 1.0f / (GetEnvironmentObj<Home>(PlayerEnvironment.Home).reduceThreatSpeedScale * SceneController.instance.Human.reduceThreatSpeed);
            }
            else
            {
                changeThreat = 1;
                updateNeedTime = 1.0f / SceneController.instance.Human.addThreatSpeed;
            }

            threatTimer += Time.fixedDeltaTime;
            while (threatTimer > updateNeedTime)
            {
                threatTimer -= updateNeedTime;
                SceneController.instance.Human.ChangeThreat(changeThreat);
            }

            if (operatorManager.currentState == PlayerOperatorState.Dead || InSafePlace())
            {
                sceneComponent.threat.SetAlpha(0);
            }
            else
            {
                var humanThreatProgress = (float)SceneController.instance.Human.currentThreat / (float)SceneController.instance.Human.maxThreat;
                sceneComponent.threat.SetAlpha(humanThreatProgress);
            }
        }

        private float poisonTimer = 0;
        private void UpdatePoison()
        {
            if (InEnvironment(PlayerEnvironment.PoisonArea))
            {
                poisonTimer += Time.fixedDeltaTime;
                float onePoisonTime = 1 / GetEnvironmentObj<PoisonArea>(PlayerEnvironment.PoisonArea).damage;
                while (poisonTimer > onePoisonTime)
                {
                    poisonTimer -= onePoisonTime;
                    Damage(1);
                }
            }
        }

        public void Damage(int value)
        {
            //冲刺时无敌
            if (isDashing) return;
            hp -= value;
            if (hp < 0)
            {
                hp = 0;
            }
            EventManager.Get<EventDefine.PlayerValueChanged>().Fire(null);
        }

        public void Heal(int value)
        {
            hp += value;
            if (hp > maxHP)
            {
                hp = maxHP;
            }
            EventManager.Get<EventDefine.PlayerValueChanged>().Fire(null);
        }

        public void AddScore(int value)
        {
            score += value;
            EventManager.Get<EventDefine.PlayerValueChanged>().Fire(null);
        }

        public void SpendScore(int value)
        {
            score -= value;
            EventManager.Get<EventDefine.PlayerValueChanged>().Fire(null);
        }

        #endregion


        #region Control
        public PlayerOperatorManager operatorManager;
        public Vector2 pos => new Vector2(sceneObject.transform.position.x, sceneObject.transform.position.y);
        public float targetRotateZ;
        public Vector2 acc;
        private Vector2 m_vel;
        public Vector2 vel
        {
            get => m_vel;
            set
            {
                //Debug.Log($"SetVel {value} {value.magnitude}");
                m_vel = value;
            }
        }
        public bool isDashing => operatorManager.currentState == PlayerOperatorState.Dash;

        public int dashMaxCount;
        public int dashCurrentCount;
        /// <summary>
        /// 当前的恢复CD
        /// </summary>
        public float dashCurrentCD;
        /// <summary>
        /// CD计时器
        /// </summary>
        public float dashCDTimer;
        /// <summary>
        /// 每一次冲刺后的CD硬直
        /// </summary>
        public float dashEndCDTimer;


        private void InitControl()
        {
            dashMaxCount = ConfigManager.instance.playerConfig.initDashCount;
            dashCurrentCount = dashMaxCount;
            dashCurrentCD = ConfigManager.instance.playerConfig.initDashCD;
            dashCDTimer = -1;
            dashEndCDTimer = -1;

            targetRotateZ = GetRotateZ();

            operatorManager = new PlayerOperatorManager(this);
            operatorManager.Init();
        }

        private void UpdateControl()
        {
            UpdateDashCD();
            UpdateDashEndCD();
            operatorManager.Update();
            UpdateRotate();
            UpdatePosition();
            UpdateAnimSpeed();
        }

        public void UpdatePosition()
        {
            if (vel.sqrMagnitude < 0.00001f) return;

            var newPos = pos + vel;
            if (CheckGroundBorder(newPos, out var boarderPos))
            {
                //往回拉0.1
                newPos = boarderPos - vel.normalized * 0.1f;
                vel = Vector2.zero;
            }
            sceneObject.transform.position = newPos;
        }

        private void UpdateRotate()
        {
            var currentRotateZ = GetRotateZ();
            if (Mathf.Abs(targetRotateZ - currentRotateZ) > 0.01f)
            {
                var newRotateZ = Mathf.LerpAngle(currentRotateZ, targetRotateZ, Time.fixedDeltaTime * ConfigManager.instance.playerConfig.rotateVel);
                //Debug.Log($"newRotateZ:{currentRotateZ}");
                sceneComponent.spriteRoot.rotation = Quaternion.Euler(0, 0, newRotateZ);
            }
        }

        public void UpdateRotate(float rotateZ)
        {
            targetRotateZ = rotateZ;
        }

        public float GetRotateZ()
        {
            return sceneComponent.spriteRoot.rotation.eulerAngles.z;
        }

        public void UseDashCount()
        {
            if (dashCurrentCount > 0)
            {
                dashCurrentCount--;
            }
        }

        /// <summary>
        /// 冲刺次数恢复
        /// </summary>
        private void UpdateDashCD()
        {
            //DashCD
            if (dashCurrentCount < dashMaxCount)
            {
                if (dashCDTimer < 0)
                {
                    //开始计时
                    dashCDTimer = dashCurrentCD;
                }
                dashCDTimer -= Time.fixedDeltaTime;
                if (dashCDTimer < 0)
                {
                    //CD好了
                    dashCurrentCount++;
                    if (dashCurrentCount < dashMaxCount)
                    {
                        //继续CD
                        dashCDTimer += dashCurrentCD;
                    }
                    else
                    {
                        //结束CD
                        dashCDTimer = -1;
                    }
                }
            }
            else
            {
                dashCDTimer = -1;
            }
        }

        /// <summary>
        /// 每一次冲刺后的CD时间
        /// </summary>
        private void UpdateDashEndCD()
        {
            if (dashEndCDTimer > 0)
            {
                dashEndCDTimer -= Time.fixedDeltaTime;
                if (dashEndCDTimer < 0)
                {
                    dashEndCDTimer = -1;
                }
            }
        }

        public bool InDashEndCD()
        {
            return dashEndCDTimer > 0;
        }

        public void StartDashEndCD()
        {
            dashEndCDTimer = ConfigManager.instance.playerConfig.dashEndCD;
            UpdateDashEndCD();
        }

        public bool CheckGroundBorder(Vector2 targetPos, out Vector2 borderPos)
        {
            //目标点还在地板里面  不需要检测新的位置
            if (Physics2D.OverlapPoint(targetPos, 1 << LayerMask.NameToLayer("Ground")) != null)
            {
                borderPos = Vector2.zero;
                return false;
            }

            //由于玩家在碰撞体里面  所以要反着来检测
            var dir = pos - targetPos;
            var hit = Physics2D.Raycast(targetPos, dir, dir.magnitude, 1 << LayerMask.NameToLayer("Ground"));
            //Debug.Log($"start:{pos} target:{targetPos} distance:{dir.magnitude}  hitDistance{hit.distance} hitPoint{hit.point}  hitCollider:{hit.collider?.name}", hit.collider.gameObject);
            borderPos = hit.point;
            return hit.distance > 0;
        }

        public bool InRangeOfSkill(HumanSkillType type)
        {
            var collider2d = Physics2D.OverlapPoint(pos, StaticUtils.SkillObjectLayer);
            return collider2d != null && collider2d.tag == type.ToString();
        }

        public bool InRangeOfCollider(Collider2D collider)
        {
            var collider2ds = Physics2D.OverlapPointAll(pos);
            foreach (var c in collider2ds)
            {
                if (c == collider) return true;
            }
            return false;
        }

        private void UpdateAnimSpeed()
        {
            if (operatorManager.currentState == PlayerOperatorState.Move)
            {
                sceneComponent.animator.speed = vel.magnitude / moveSpeed;
            }
            else
            {
                sceneComponent.animator.speed = 1;
            }
        }

        public void PlayAnim(string animName)
        {
            sceneComponent.animator.Play(animName);
        }

        #endregion
    }
}