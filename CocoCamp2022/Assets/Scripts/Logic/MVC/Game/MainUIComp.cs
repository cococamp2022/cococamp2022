using UnityEngine;
using UnityEngine.UI;
namespace Logic
{
    public class MainUIComp : MonoBehaviour
    {
        public Slider blood;
        public Text bloodText;
        public Slider myFood;
        public Text myFoodText;
        public Slider foodState;
        public Text foodStateText;
        public MultiItemManager dashCount;
    }
}