using UnityEngine;
using UnityEngine.UI;
namespace Logic
{
    public class HudUIComp:MonoBehaviour
    {
        public Image hud;

        public float colorChangeSpeed = 5;
        public int dangerHP = 30;
        public Color poisonColor;
        public Color safeColor;
        public Color dangerColor;
        public Color eatingColor;
        public Color normalColor;
    }
}