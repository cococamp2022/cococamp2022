using UnityEngine;
using UnityEngine.UI;
using Config;

namespace Logic
{
    public class HudUI : BaseUI<HudUIComp, Player>
    {
        protected override string prefabName => "HUD";

        protected override UISort sort => UISort.HUD;
        protected override void OnInit()
        {
        }

        protected override void OnShow()
        {
            if (SceneController.instance.realHaveTwoPlayer)
            {
                //稍微拉长一点  应对变形
                sceneObject.GetComponent<RectTransform>().sizeDelta = new Vector2(960, 1200);
                if (data.tag == "Player1")
                {
                    sceneObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(-480, 0);
                }
                else
                {
                    sceneObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(480, 0);
                }
            }
            else
            {
                sceneObject.GetComponent<RectTransform>().sizeDelta = new Vector2(1920, 1080);
                sceneObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
            }
        }

        protected override void OnHide()
        {

        }

        protected override void OnDestroy()
        {

        }
        protected override void RegisterEvents()
        {
        }
        protected override void UnRegisterEvents()
        {

        }
        protected override void OnClick(GameObject obj)
        {

        }

        protected override void OnUpdate()
        {
            Color newColor = new Color(0, 0, 0, 0);
            if (data.InEnvironment(PlayerEnvironment.PoisonArea))
            {
                newColor = sceneComponent.poisonColor;
            }
            // else if (SceneController.instance.Human.AboveAttackThreat)
            // {
            //     var human = SceneController.instance.Human;
            //     newColor = sceneComponent.dangerColor;
            //     float a = (float)(human.currentThreat - human.attackThreat) / (float)(human.maxThreat - human.attackThreat);
            //     newColor = new Color(newColor.r, newColor.g, newColor.b, a);
            // }
            else if (data.hp < sceneComponent.dangerHP)
            {
                newColor = sceneComponent.dangerColor;
                float a = (float)(sceneComponent.dangerHP - data.hp) / (float)(sceneComponent.dangerHP);
                newColor = new Color(newColor.r, newColor.g, newColor.b, a);
            }
            else if (data.operatorManager.currentState == PlayerOperatorState.Eat)
            {
                newColor = sceneComponent.eatingColor;
            }
            else if (data.InSafePlace())
            {
                newColor = sceneComponent.safeColor;
            }
            else
            {
                newColor = sceneComponent.normalColor;
            }
            newColor = Color.Lerp(sceneComponent.hud.color, newColor, Time.fixedDeltaTime * sceneComponent.colorChangeSpeed);
            sceneComponent.hud.color = newColor;
        }
    }
}