using Config;
using UnityEngine;
using UnityEngine.UI;

namespace Logic
{
    public class IntroUI : BaseUI
    {
        protected override UISort sort => UISort.IntroUI;

        protected override string prefabName => "IntroUI";
        private MultiItemManager levelBtnList;
        private LevelConfigItem[] levelList;

        protected override void OnClick(GameObject obj)
        {
            switch (obj.name)
            {
                case "btnStart":
                    GameController.instance.LoadLevel("Level1", true);
                    break;
                case "1":
                    //GameController.instance.LoadLevel("Level1", false);
                    GameController.instance.ShowUI(UISort.SelectCharactorUI);
                    Hide();
                    break;
                case "2":
                    GameController.instance.LoadLevel("Level1", true);
                    break;
            }
            if (obj.name.StartsWith("Button"))
            {
                GameController.instance.LoadLevel(obj.GetChild<Text>("Text").text);
            }
        }

        protected override void OnInit()
        {
            levelList = ConfigManager.instance.levelConfig.levelList;
            levelBtnList = sceneObject.GetChild<MultiItemManager>("bg/levelList");
            levelBtnList.SetSize(levelList.Length);
            for (int i = 0; i < levelList.Length; i++)
            {
                var btnTransform = levelBtnList.GetItem<Transform>(i);
                btnTransform.Find("Text").GetComponent<Text>().text = levelList[i].sceneName;
            }
            ResetAllClicks();
        }

        protected override void OnShow()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnDestroy()
        {

        }
    }
}