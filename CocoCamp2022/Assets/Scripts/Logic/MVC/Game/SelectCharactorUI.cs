using UnityEngine;
using Config;

namespace Logic
{
    public class SelectCharactorUI : BaseUI
    {

        protected override UISort sort => UISort.SelectCharactorUI;
        protected override string prefabName => "SelectCharactorUI";

        protected override void OnInit()
        {

        }

        protected override void OnShow()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnClick(GameObject obj)
        {
            switch (obj.name)
            {
                case "btn1":
                    GameController.instance.LoadLevel("Level1", false, 1);
                    break;
                case "btn2":
                    GameController.instance.LoadLevel("Level1", false, 2);
                    break;
            }
            Destroy();
        }
    }
}