using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Logic
{
    public class GameController : BaseController<GameController, GameModel>
    {
        private GameOperatorManager operatorManager;
        protected override void OnInit()
        {
            operatorManager = new GameOperatorManager();
        }
        protected override void RegisterAllUI()
        {
            RegisterUI(UISort.MainUI, new MainUI());
            RegisterUI(UISort.IntroUI, new IntroUI());
            RegisterUI(UISort.GameUI, new GameUI());
            RegisterUI(UISort.DeadUI, new DeadUI());
            RegisterUI(UISort.SuccessUI, new SuccessUI());
            RegisterUI(UISort.SelectCharactorUI, new SelectCharactorUI());
        }

        protected override void RegisterEvents()
        {
            EventManager.Get<EventDefine.ModulesInitFinished>().AddListener(OnModulesInitFinished);
        }

        protected override void UnRegisterEvents()
        {
            EventManager.Get<EventDefine.ModulesInitFinished>().RemoveListener(OnModulesInitFinished);
        }

        protected override void OnFixedUpdate()
        {
            if (Keyboard.current.escapeKey.wasPressedThisFrame)
            {
                if (GetUI<GameUI>(UISort.GameUI).isShow)
                {
                    HideUI(UISort.GameUI);
                }
                else
                {
                    ShowUI(UISort.GameUI);
                }
            }
        }

        private void OnModulesInitFinished()
        {
            operatorManager.Init();
        }

        public void LoadLevel(string levelID, bool towPlayer = false, int charactorIndex = 1)
        {
            SceneController.instance.initWithTwoPlayer = towPlayer;
            SceneController.instance.initPlayerIndex = charactorIndex;
            operatorManager.ChangeState(GameOperatorState.Level, new GameOperatorParam_Level() { levelID = levelID });
        }

        public void ChangeToIntro()
        {
            operatorManager.ChangeState(GameOperatorState.Intro);
        }
    }
}