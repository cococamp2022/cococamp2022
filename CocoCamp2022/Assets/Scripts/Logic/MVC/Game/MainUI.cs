using UnityEngine;
using UnityEngine.UI;
using Config;

namespace Logic
{
    public class MainUI : BaseUI<MainUIComp, Player>
    {
        protected override string prefabName => "MainUI";

        protected override UISort sort => UISort.MainUI;
        protected override void OnInit()
        {
        }

        protected override void OnShow()
        {
            if (SceneController.instance.realHaveTwoPlayer)
            {
                sceneObject.GetComponent<RectTransform>().sizeDelta = new Vector2(960, 1080);
                if (data.tag == "Player1")
                {
                    sceneObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(-480, 0);
                }
                else
                {
                    sceneObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(480, 0);
                }
            }
            else
            {
                sceneObject.GetComponent<RectTransform>().sizeDelta = new Vector2(1920, 1080);
                sceneObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
            }
        }

        protected override void OnHide()
        {

        }

        protected override void OnDestroy()
        {

        }
        protected override void RegisterEvents()
        {
        }
        protected override void UnRegisterEvents()
        {

        }

        protected override void OnUpdate()
        {
            sceneComponent.blood.value = (float)data.hp / (float)data.maxHP;
            sceneComponent.bloodText.text = $"{data.hp}/{data.maxHP}";
            sceneComponent.myFood.value = (float)data.score / (float)Config.ConfigManager.instance.gameConfig.successFood;
            sceneComponent.myFoodText.text = $"{data.score}/{Config.ConfigManager.instance.gameConfig.successFood}";
            if (data.InEnvironment(PlayerEnvironment.Food))
            {
                sceneComponent.foodState.gameObject.SetActiveOptimize(true);
                var food = data.GetEnvironmentObj<Food>(PlayerEnvironment.Food);
                sceneComponent.foodState.value = (float)food.foodCount / (float)food.maxFoodCount;
                sceneComponent.foodStateText.text = $"{food.foodCount}/{food.maxFoodCount}";
            }
            else
            {
                sceneComponent.foodState.gameObject.SetActiveOptimize(false);
            }
            sceneComponent.dashCount.SetSize(data.dashCurrentCount);
        }
        protected override void OnClick(GameObject obj)
        {

        }
    }
}