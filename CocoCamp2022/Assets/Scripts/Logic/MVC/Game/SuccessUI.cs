using UnityEngine;
using Config;

namespace Logic
{
    public class SuccessUI : BaseUI
    {

        protected override UISort sort => UISort.SuccessUI;
        protected override string prefabName => "SuccessUI";

        protected override void OnInit()
        {
        }

        protected override void OnShow()
        {
            if (SceneController.instance.realHaveTwoPlayer)
            {
                sceneObject.GetChild("1").SetActiveOptimize(false);
                sceneObject.GetChild("2").SetActiveOptimize(true);
            }
            else
            {
                sceneObject.GetChild("1").SetActiveOptimize(true);
                sceneObject.GetChild("2").SetActiveOptimize(false);
            }
        }

        protected override void OnHide()
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnClick(GameObject obj)
        {
            switch (obj.name)
            {
                case "restart":
                    GameController.instance.ChangeToIntro();
                    Hide();
                    break;
            }

        }
    }
}