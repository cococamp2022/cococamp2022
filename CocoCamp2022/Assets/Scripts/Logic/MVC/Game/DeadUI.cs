using UnityEngine;
using Config;

namespace Logic
{
    public class DeadUI : BaseUI
    {

        protected override UISort sort => UISort.DeadUI;
        protected override string prefabName => "DeadUI";

        protected override void OnInit()
        {
        }

        protected override void OnShow()
        {

        }

        protected override void OnHide()
        {

        }

        protected override void OnDestroy()
        {

        }

        protected override void OnClick(GameObject obj)
        {
            switch (obj.name)
            {
                case "restart":
                    if (SceneController.instance.currentLevelID.IsFilled())
                    {
                        GameController.instance.LoadLevel(SceneController.instance.currentLevelID, SceneController.instance.initWithTwoPlayer);
                        Hide();
                    }
                    break;
            }

        }
    }
}