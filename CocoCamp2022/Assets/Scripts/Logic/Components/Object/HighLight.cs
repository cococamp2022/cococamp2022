using System.Collections.Generic;
using ObjectPool;
using UnityEngine;

namespace Logic
{
    public class HighLight : MonoBehaviour
    {
        public static void Set(GameObject obj, bool value)
        {
            HighLight highLight = obj.GetComponent<HighLight>();
            if (highLight == null) highLight = obj.AddComponent<HighLight>();
            highLight.SetHighLight(value);
        }

        private static Material highLightMat;
        private Dictionary<Renderer, Material> originalMat;
        private bool isHighLight = false;

        protected void SetHighLight(bool value, float alpha)
        {
            SetHighLight(value);

        }
        
        protected void SetHighLight(bool value)
        {
            if (isHighLight == value) return;

            isHighLight = value;
            var renders = transform.GetComponentsInChildren<Renderer>();
            if (isHighLight)
            {
                originalMat ??= new Dictionary<Renderer, Material>();
                highLightMat ??= ObjectPoolManager.instance.MaterialPool.Pop("HighLight");
                originalMat.Clear();
                foreach (var render in renders)
                {
                    originalMat.Add(render, render.sharedMaterial);
                    render.sharedMaterial = highLightMat;
                }
            }
            else
            {
                foreach (var pair in originalMat)
                {
                    pair.Key.sharedMaterial = pair.Value;
                }
                originalMat.Clear();
            }
        }
    }
}
