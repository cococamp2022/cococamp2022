using UnityEngine;

namespace Logic
{
    public static class SpriteRendererExtension
    {
        public static void SetAlpha(this SpriteRenderer renderer, float alpha)
        {
            var newColor = new Color(renderer.color.r, renderer.color.g, renderer.color.b, alpha);
            renderer.color = newColor;
        }
    }
}