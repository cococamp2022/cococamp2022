using UnityEngine;

namespace Logic

{
    public static class Collider2DExtension
    {
        public static GameObject GetRootGO(this Collider2D collider2D)
        {
            return collider2D.GetComponent<ColliderHandler>()?.root;
        }

        public static T GetRootGO<T>(this Collider2D collider2D) where T : class
        {
            return collider2D.GetComponent<ColliderHandler>()?.root?.GetComponent<T>();
        }

        public static Player GetPlayer(this Collider2D collider2D)
        {
            return collider2D.GetComponent<ColliderHandler>()?.root?.GetComponent<PlayerComponent>()?.player;
        }
    }
}