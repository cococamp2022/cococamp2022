namespace Logic
{
    public static class StringExtension
    {
        public static bool IsNullOrEmpty(this string s)
        {
            return string.IsNullOrEmpty(s);
        }

        public static bool IsFilled(this string s)
        {
            return !string.IsNullOrEmpty(s);
        }
    }
}