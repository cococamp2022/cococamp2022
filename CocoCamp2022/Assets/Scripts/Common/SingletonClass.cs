using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class SingletonClass<T> : object where T : new()
{
    private static T s_Instance = default(T);
    private static object s_syncObj = new object();
    public static T instance
    {
        get
        {
            if (s_Instance == null)
            {
                lock (s_syncObj)
                {
                    if (s_Instance == null)
                    {
                        s_Instance = new T();
                    }
                }
            }
            return s_Instance;
        }
    }
}