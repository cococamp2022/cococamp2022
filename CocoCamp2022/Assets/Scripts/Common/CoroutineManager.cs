using UnityEngine;
using System.Collections;
using System;

namespace Logic
{
    public class CoroutineManager : SingletonMonoBehaviourClass<CoroutineManager>
    {
        public Coroutine TryStartCoroutine(IEnumerator enumerator)
        {
            return StartCoroutine(wrap(enumerator, false));
        }

        static IEnumerator wrap(IEnumerator enumerator, bool silent)
        {
            while (true)
            {
                try
                {
                    if (!enumerator.MoveNext())
                        break;
                }
                catch (Exception e)
                {
                    if (!silent)
                        Debug.LogException(e);
                    break;
                }
                yield return enumerator.Current;
            }
        }
    }
}