using UnityEngine;
using System.Collections;
using System;

namespace Cameras
{
    public class CameraManager : SingletonMonoBehaviourClass<CameraManager>
    {
        public SceneCamera camera1;
        public SceneCamera camera2;
    }
}