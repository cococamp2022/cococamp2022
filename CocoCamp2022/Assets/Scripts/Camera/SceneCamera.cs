using UnityEngine;
using Cinemachine;

namespace Cameras
{
    public class SceneCamera : MonoBehaviour
    {
        public Camera cameraObject;
        public CinemachineBrain brain;
        public CinemachineVirtualCamera activeVC;
        void Awake()
        {
            cameraObject = transform.GetComponent<Camera>();
        }

        public void SetTarget(Transform target)
        {
            activeVC.LookAt = target;
            activeVC.Follow = target;
        }

        public void SetConfiner(Collider2D collider2D)
        {
            activeVC.GetComponent<CinemachineConfiner>().m_BoundingShape2D = collider2D;
        }

        public void SetSide(bool left)
        {
            if(left)
            {
                cameraObject.rect = new Rect(0, 0, 0.5f, 1);
            }
            else
            {
                cameraObject.rect = new Rect(0.5f, 0, 0.5f, 1);
            }
        }

        public void SetNormal()
        {
            cameraObject.rect = new Rect(0, 0, 1, 1);
        }
    }
}