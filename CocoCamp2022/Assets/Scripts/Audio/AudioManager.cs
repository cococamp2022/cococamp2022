using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Logic;

public class AudioManager : SingletonMonoBehaviourClass<AudioManager>
{
    /// <summary>
    /// 所有的音频索引  可以将音频拖动到GameObject实例的脚本上
    /// </summary>
    public AudioClip[] clips;
    /// <summary>
    /// 背景音播放器
    /// </summary>
    public AudioPlayer BGM;
    /// <summary>
    /// 环境音播放器
    /// </summary>
    public AudioPlayer AGM;
    /// <summary>
    /// Effect播放器的管理器  播完即停  可重复叠加多个相同的Effect
    /// </summary>
    public MultiItemManager EffectList;
    /// <summary>
    /// SingletonEffectList播放器的管理器  播完即停  每个Effect只会有一个实例
    /// </summary>
    public MultiItemManager SingletonEffectList;
    private Dictionary<string, AudioClip> m_audioDict;
    /// <summary>
    /// 由音频名字索引音频的Dict
    /// </summary>
    private Dictionary<string, AudioClip> audioDict
    {
        get
        {
            if (m_audioDict == null)
            {
                m_audioDict = new Dictionary<string, AudioClip>();
                foreach (var clip in clips)
                {
                    audioDict.Add(clip.name, clip);
                }
            }
            return m_audioDict;
        }
    }

    private Dictionary<string, AudioPlayer> singletonEffectDict;


    protected override void onAwake()
    {
        EffectList.SetSize(1);
        singletonEffectDict = new Dictionary<string, AudioPlayer>();
    }

    /// <summary>
    /// 播放Effect音效  可以重复播放相同音效
    /// </summary>
    /// <param name="name"></param>
    public void PlayEffect(string name)
    {
        if (audioDict.TryGetValue(name, out AudioClip clip))
        {
            for (int i = 0; i < EffectList.Size; i++)
            {
                var source = EffectList.GetItem<AudioSource>(i);
                if (!source.isPlaying)
                {
                    source.clip = clip;
                    source.Play();
                    return;
                }
            }

            var newSource = EffectList.Expand().GetComponent<AudioSource>();
            newSource.clip = clip;
            newSource.Play();
        }
    }
    private bool isPlayingBGM;

    public void PlayBGM(string name)
    {
        if (audioDict.TryGetValue(name, out AudioClip clip))
        {
            isPlayingBGM = true;
            BGM.Play(clip, true);
        }
    }

    public void StopBGM(bool forceChange = false)
    {
        if (forceChange || isPlayingBGM)
        {
            isPlayingBGM = false;
            BGM.Stop();
        }
    }

    public void PlayAGM(string name)
    {
        if (audioDict.TryGetValue(name, out AudioClip clip))
        {
            AGM.Play(clip, true);
        }
    }

    public void StopAGM()
    {
        AGM.Stop();
    }

    public void PlaySingletonEffect(string name, bool loop = false, float fadeTime = 0.1f)
    {
        if (audioDict.TryGetValue(name, out AudioClip clip))
        {
            if (!singletonEffectDict.ContainsKey(name))
            {
                singletonEffectDict.Add(name, SingletonEffectList.Expand().GetComponent<AudioPlayer>());
            }
            if (!singletonEffectDict[name].isPlaying)
            {
                singletonEffectDict[name].Play(clip, loop, fadeTime);
            }
        }
    }

    public void StopSingletonEffect(string name, float fadeTime = 0.2f)
    {
        if (audioDict.TryGetValue(name, out AudioClip clip))
        {
            if (singletonEffectDict.ContainsKey(name))
            {
                if (singletonEffectDict[name].isPlaying)
                {
                    singletonEffectDict[name].Stop(fadeTime);
                }
            }
        }
    }

    void Update()
    {

    }
}