using UnityEngine;
using UnityEngine.InputSystem;
using Logic;
using Logic.EventDefine;
namespace Input
{
    public enum PlayerIndex
    {
        Player1,
        Player2
    }
    public class InputManager : SingletonMonoBehaviourClass<InputManager>
    {
        public PlayerInput playerInput1;
        public PlayerInput playerInput2;
        protected override void onAwake()
        {
            InputSystem.onDeviceChange += onDeviceChange;
        }

        private void onDeviceChange(InputDevice device, InputDeviceChange deviceChange)
        {
            //TODO  刷新玩家的输入设备绑定
        }

        public PlayerInput GetInputByTag(string tag)
        {
            if(tag == "Player1")
            {
                return playerInput1;
            }
            else if(tag == "Player2")
            {
                return playerInput2;
            }
            return playerInput1;
        }
    }
}